#ifndef _GLOBAL_H
#define _GLOBAL_H

#define RET_OK		0
#define RET_ERR		( RET_OK - 1 )

#define COMM_BUFFER_MAX_LEN	10240

#define DATACONTAINER_SIZE_10K	10240
#define DATACONTAINER_SIZE_8192	8192
#define DATACONTAINER_SIZE_4096	4096
#define DATACONTAINER_SIZE_2048	2048
#define DATACONTAINER_SIZE_1024	1024
#define DATACONTAINER_SIZE_512	512
#define DATACONTAINER_SIZE_256	256
#define DATACONTAINER_SIZE_128	128
#define DATACONTAINER_SIZE_64	64
#define DATACONTAINER_SIZE_32	32
#define	DATACONTAINER_SIZE_16	16
#define DATACONTAINER_SIZE_8	8

#define DATACONTAINER_ELEMENTS_MAX	64

#define PAYLOADNAME_SIZE_MAX	128
#define PAYLOADVALUE_SIZE_MAX	256
#define PAYLOAD_ELEMENTS_MAX	64

#define CHAR_SENTINEL_START		"*^"
#define CHAR_SENTINEL_END		"^*"

typedef struct dataContainer
{
	int		payloadSize ;
	char	cmdID[DATACONTAINER_SIZE_128] ;
	char	payloadName[DATACONTAINER_ELEMENTS_MAX][DATACONTAINER_SIZE_128];
	char	payloadData[DATACONTAINER_ELEMENTS_MAX][DATACONTAINER_SIZE_256];
	char	postBackUri[DATACONTAINER_SIZE_256];
	//emv fields
	char	message[DATACONTAINER_SIZE_256] ;
	char	ksn[DATACONTAINER_SIZE_64] ;
	char	payload[DATACONTAINER_ELEMENTS_MAX][DATACONTAINER_SIZE_256];
	char	paymentRequest[DATACONTAINER_ELEMENTS_MAX][DATACONTAINER_SIZE_256];
	char	sessionId[DATACONTAINER_SIZE_256];
	//payment fields
	char	transactionAmount[DATACONTAINER_SIZE_32] ;
	char	latitude[DATACONTAINER_SIZE_8] ;
	char	longitude[DATACONTAINER_SIZE_8] ;
	char	currencyCode[DATACONTAINER_SIZE_8] ;
	char	tip[DATACONTAINER_SIZE_16] ;
	char	emvChipData[DATACONTAINER_SIZE_256] ;
	char	posCondition[DATACONTAINER_SIZE_8] ;
	char	AID[DATACONTAINER_SIZE_32] ;
	char	cardSequenceNumber[DATACONTAINER_SIZE_8] ;
	char	cardholderName[DATACONTAINER_SIZE_64] ;
	char	entryMode[DATACONTAINER_SIZE_8] ;
	char	panToken[DATACONTAINER_SIZE_64] ;
	char	requestTimestamp[DATACONTAINER_SIZE_32] ;
	char	tc[DATACONTAINER_SIZE_64] ;
	char	track2Data[DATACONTAINER_SIZE_32] ;
	char	aidScheme[DATACONTAINER_SIZE_32] ;
	char	approvalCode[DATACONTAINER_SIZE_8] ;
	char	batchID[DATACONTAINER_SIZE_8] ;
	char	branchID[DATACONTAINER_SIZE_64] ;
	char	ID[DATACONTAINER_SIZE_64] ;
	char	invoiceNum[DATACONTAINER_SIZE_8];
	char	maskedPAN[DATACONTAINER_SIZE_16];
	char	mid[DATACONTAINER_SIZE_16];

	char	rrn[DATACONTAINER_SIZE_16];
	char	respCode[DATACONTAINER_SIZE_8] ;
	char	stan[DATACONTAINER_SIZE_8] ;
	char	status[DATACONTAINER_SIZE_64] ;
	char	termID[DATACONTAINER_SIZE_16] ;
	char	timeStamp[DATACONTAINER_SIZE_64] ;
	char	tranType[DATACONTAINER_SIZE_8] ;

	char	userName[DATACONTAINER_SIZE_32];
	char	passWord[DATACONTAINER_SIZE_32];
	char	vtID[DATACONTAINER_SIZE_16];
}g_dataContainerBuffer ;

typedef struct paramContainerObj
{
   char terminalID[DATACONTAINER_SIZE_16] ;
   char terminalName[DATACONTAINER_SIZE_256] ; 
   char primaryIPHost[DATACONTAINER_SIZE_16] ;
   int  primaryPortHost ;
   char secondaryIPHost[DATACONTAINER_SIZE_16];
   int  secondaryPortHost ;
   char gprsAPN[DATACONTAINER_SIZE_128] ;
   int  dhcpFlag ;
   char termIPAddress[DATACONTAINER_SIZE_16] ;
   char termSubnet[DATACONTAINER_SIZE_16] ;
   char termGateway[DATACONTAINER_SIZE_16] ;
   char termDNS1[DATACONTAINER_SIZE_16] ;
   char termDNS2[DATACONTAINER_SIZE_16] ;
   int  termNII ;
   char gprsUsername[DATACONTAINER_SIZE_128] ;
   char gprsPassword[DATACONTAINER_SIZE_128] ;

}g_paramContainerBuffer ;

g_dataContainerBuffer g_dataContainer ;
g_paramContainerBuffer g_paramContainer ;

#endif
#ifndef ENCRYPTION_H
#define ENCRYPTION_H

#include <posapi.h>

#define KEY_LENGTH		16 //密钥长度
#define PINBLOCK_LEN	8
#define MAC_DATA_LEN	16 //进行MAC计算的数据长度
#define MACBLOCK_LEN	8
#define TDK_DATA_LEN	16 //进行TDK计算的数据长度

#define PAN_LEN			16 //卡号长度
#define KEY_VAR_LEN     "4, 5, 6, 7, 8"     //可输入PIN长度
#define KSN_LEN			10
#define KEY_NUM         6                   //密钥类型个数TLK.TMK.TPK.TAK.TDK.TIK

#define TLK_VALUE_IN	"0123456789123456" //TLK密钥密文
#define TMK_VALUE_IN	"2222222222222222" //TMK密钥密文
#define TIK_VALUE_IN	"3333333333333333" //TIK密文
#define TPK_VALUE_IN	"4111111111111111" //TPK密文
#define TAK_VALUE_IN	"4222222222222222" //TAK密文
#define TDK_VALUE_IN	"4333333333333333" //TDK ciphertext
#define KSN_VALUE_IN	"0000000001"	   //initial KSN

#define PAN_VALUE_IN	"6666666666666666" //16位卡号 

uchar g_uckeyNum[KEY_NUM];
uchar szPINBlock[PINBLOCK_LEN + 1];		//store the encrypted PINBlock
uchar szMacBlock[MACBLOCK_LEN + 1];		//store the encrypted MacData
uchar szTDKBlock[TDK_DATA_LEN + 1];		//store the encrypted TDKData
uchar szPINBlockDukpt[PINBLOCK_LEN + 1];
uchar szMacBlockDukpt[MACBLOCK_LEN + 1];

uchar szKSNOut[KSN_LEN+1];

int flag;

ST_KEY_INFO	stKeyInfoIn/*[KEY_NUM]*/;
ST_KCV_INFO	stKcvInfoIn/*[KEY_NUM]*/;

void testEncryption( void ) ;

#endif
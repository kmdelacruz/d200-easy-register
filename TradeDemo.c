/********************************************
* CopyRight by 
* PAX Computer Technology(SHENZHEN) Co.Ltd
*
* File Description:Trade Demo
* 20120407
********************************************/

#include <posapi.h>
#include <string.h>
#include "TradeDemo.h"
#include "stdarg.h"
#include "bluetooth.h"
#include "apduCmd.h"


uchar PORTOPEN_PARA[20];

extern uchar gucTerminalInfo[30];
extern int wifiStatus;

int giLCDMAX_X = 0;
int giLCDMAX_Y = 0;

uchar s_RecvBuf[10240] = {0};
uchar s_SendBuf[1024] = {0};
ulong s_lSendBufLen = 0;
int size;
int listenSocket = -1;
int Socket = -1;   //connection socket
uchar WFflag=0;//wifi flag
uchar BTflag=0;//BT flag

uchar amount[32]={0};
uchar host_ip[64] = {0};

uchar gucComPort=0;

ST_WIFI_PARAM glWifiParam;
ST_WIFI_AP glApParam;


enum TImageFileType
{
	tiftBmp,
	tiftJpeg,
	tiftPng,
	tiftGif,
};
enum TImageCmdType
{
	tictLoad,
	tictDisplay,
	tictStop,
	tictClean,
};



int ScrProcessImage(char* FileName, int FileType, int cmd, int x, int y);
void DisplayBang();
void DisplayWifi();
void DisplayBT();
void DispPassWordInput();
int CheckLanguage(void);
extern int s_StringToHex(char *HexString, uchar *hex);

void WaitingStateEngine(void);

#define COMPORT COM1

int ComSends(const char * Str,...)
{
  #ifdef _DEBUG_
	va_list vaMarker;
	char TheBuff[2048];
	int StrLen;
	uchar ucRet;
	va_start(vaMarker, Str); 
    StrLen = vsprintf(TheBuff,(const char*)Str,vaMarker); 
	va_end(vaMarker);
	TheBuff[StrLen] = 0x0D;
	TheBuff[StrLen+1] = 0x0A;
	TheBuff[StrLen+2] = 0x00;
	StrLen += 2;
	//ucRet = PortOpen(COMPORT,"115200,8,n,1");
	//if(ucRet ==0)
	
	{
		PortSends(COMPORT,TheBuff,(ushort)StrLen);
		while(PortTxPoolCheck(COMPORT));
		//PortClose(COMPORT);
	}
#endif
	
	return 0;

	
}


void demo()
{
	int iRet =0;    
	iRet= ScrProcessImage("001.png",tiftPng,tictLoad,0,0); //001
	Lcdprintf("\n iRet0= %d",iRet);
	getkey();
	ScrProcessImage("upload.png",tiftPng,tictLoad,0,0);
	ScrProcessImage("TimeSet.png",tiftPng,tictLoad,0,0);
		
	ScrProcessImage("002.png",tiftPng,tictLoad,0,0);//002
	ScrProcessImage("003.png",tiftPng,tictLoad,0,0);//003
	ScrProcessImage("004.png",tiftPng,tictLoad,0,0);//004
	ScrProcessImage("005.png",tiftPng,tictLoad,0,0);//004
	ScrProcessImage("006.png",tiftPng,tictLoad,0,0);//004

	while(1)
	{
		iRet= ScrProcessImage("001.png",tiftPng,tictDisplay,0,0); //001
		//Lcdprintf("\n iRet1= %d",iRet);
		getkey();
		ScrProcessImage("002.png",tiftPng,tictDisplay,0,0);//002
		getkey();
		ScrProcessImage("003.png",tiftPng,tictDisplay,0,0);//003
		getkey();
		ScrProcessImage("004.png",tiftPng,tictDisplay,0,0);//004
		getkey();
		ScrProcessImage("005.png",tiftPng,tictDisplay,0,0);//006
		getkey();
		ScrProcessImage("006.png",tiftPng,tictDisplay,0,0);//006
		getkey();
		ScrCls();
		ScrProcessImage("upload.png",tiftPng,tictDisplay,64,76);
		ScrProcessImage("TimeSet.png",tiftPng,tictDisplay,164,76);
		getkey();
	}
}


void xml_strlwr(char * turned)
{
	int i;
	i=0;
	while (turned[i]!=0)
	{
		if  ((turned[i]>='A')&&(turned[i]<='Z'))
		{
			turned[i]+=32;
		}
		i+=1;
	}
}
int xml_find(uchar *doc, int doc_len, char *find_flag, int find_flag_len, int *find_location);

/*******************************************************************************************
  int XmlAddElement(char * xml_doc,char *ele_name,char *ele_value,int value_len,int *xml_real_len)

  参数说明
  xml_doc         : xml document 名
  xml_doc_max_len : xml_doc buffer最大长度
  ele_name        : 如果是root则新建一个xml document，xml_doc 指向该xml document。value_len,ele_value任意，
                    ×xml_real_len ：为新建的xml document 的实际长度。
                    如果不是root则在xml_doc最后（</root>前)加入一个标签和标签值为ele_value 的前value_len个字符。
  ele_value       : 要新建的标签值的指针。
  value_len       : 新建标签值的总长度，程序拷贝ele_value中的value_len到xml_doc中
  xml_real_len    : xml_doc 的总长度 调用时使用是xml_doc的长度，调用成功后返回的是xml_doc的新长度。

*******************************************************************************************/
int XmlAddElement(uchar *xml_doc, int xml_doc_max_len, char *ele_name, uchar *ele_value, int value_len, int *xml_real_len)
{
	char *insert_location;
	int name_len,xml_doc_len,i,find_location[1];
	char tmp[ELE_NAME_MAX+1];

	
	if (ele_name[0]==0) return (101);

	name_len=strlen(ele_name);
	xml_strlwr(ele_name);
	
	if(name_len>ELE_NAME_MAX)
	{
		#ifdef   DEBUGON
		printf("ERROR!The Element's Name is TOO LONG or It is not end by normal!\n");
		#endif
		return(102);
	}
	if  (value_len>XML_DOC_MAX)
	{
		#ifdef   DEBUGON
		printf("The length of element value is TOO LONG\n");
		#endif
		return(103);
	}

	if ((strcmp(ele_name,"root")==0)||(strcmp(ele_name,"ROOT")==0))
	{
		strcpy(xml_doc,"<?xml version=\"1.0\"?><root></root>");
		*xml_real_len=strlen("<?xml version=\"1.0\"?><root></root>");
		return(0);
	}
	else
	{
		xml_doc_len=*xml_real_len;/*add by prs 20040116 */
		if ((xml_doc_len>XML_DOC_MAX)||(xml_doc_len<0))
		{	
			#ifdef   DEBUGON
			printf("ERROR!The xml_document length=[%d] is 0 too long !\n",xml_doc_len);
			#endif
			return(100);
		}	
		if (xml_doc[0]==0)
		{
			#ifdef   DEBUGON
			printf("ERROR!The xml_document is NULL!\n");
			#endif
			return(104);
		}


		if (xml_find(xml_doc,xml_doc_len,"</root>",7,find_location))
			return(105);

		xml_doc_len=find_location[0]+7;

		if (name_len+name_len+5+value_len+xml_doc_len>XML_DOC_MAX)
		{
			#ifdef   DEBUGON
			printf("ERROR!The Length of xml_document will TOO LONG!\n");
			#endif
			return(106);
		}
		memcpy(tmp,"<",1);
		memcpy(tmp+1,ele_name,name_len);
		memcpy(tmp+1+name_len,">",1);
		tmp[name_len+2]=0;
		if (!xml_find(xml_doc,xml_doc_len,tmp,2+name_len,find_location))
		{
			#ifdef   DEBUGON
			printf("ERROR!The Length of xml_element_name repeat !\n");
			#endif
			return(107);
		}
		/* 判断是否标签名是否已经存在 */
	
		if (name_len+name_len+5+value_len+xml_doc_len>xml_doc_max_len)
		{
			#ifdef   DEBUGON
			printf("ERROR!xml_doc over flow!\n");
			#endif
			return(108);
		}	
		*xml_real_len=name_len+name_len+5+value_len+xml_doc_len;
		
		insert_location=xml_doc+find_location[0];

		/*定位指针到xml文档的最后*/

		memcpy(insert_location,"<",1);
		insert_location+=1;
		memcpy(insert_location,ele_name,name_len);
		insert_location+=name_len;
		memcpy(insert_location,">",1);
		insert_location+=1;
		/*添加新的标签名 add new element name start*/

		memcpy(insert_location,ele_value,value_len);
		insert_location+=value_len;
		/*添加新的值,add new element value*/
		memcpy(insert_location,"</",2);
		insert_location+=2;
		memcpy(insert_location,ele_name,name_len);
		insert_location+=name_len;
		memcpy(insert_location,">",1);
		insert_location+=1;
		/*添加新标签名的结束符 add new element name end*/
		memcpy(insert_location,"</root>",7);
		insert_location+=7;
		insert_location[0]=0;

		return(0);
	}
}





/**************************************************************************
  int xml_find(char * doc,char *find_flag,int find_flag_len,int *find_location)
doc		:	被查找的字符串
doc_len		:	被查找的字符串的长度
find_flag	:	要找的字符串（find_flag中可以有任何的字符，包括'\0'
find_flag_len	:	find_flag的长度
find_location 	:	find_flag的第一个字符 在doc中的位置
**************************************************************************/
int xml_find(uchar *doc, int doc_len, char *find_flag, int find_flag_len, int *find_location)
{
	int flag,i,j;
	char temp[200],p[XML_DOC_MAX+1];

	flag=0;

	for(i=0;(i<doc_len-find_flag_len+1)&&(flag==0);i++)
	{
		memcpy(temp, doc+i, find_flag_len);
		temp[find_flag_len] = 0;
		xml_strlwr(temp);     
		if (memcmp(temp,find_flag,find_flag_len)==0)
	          	flag=1;
	}

	if (flag)
	{
		*find_location=i-1;
		#ifdef  TESTFIND
			printf("location: ");
			printf("%d\n",*find_location);
	
			printf("find succ");
			printf("%s\n",find_flag);
		#endif
		return(0);
	}
	else
	{
		#ifdef  TESTFIND
			printf("find fail  :"); printf("%s\n",find_flag);
		
		#endif
		return(1);
	}
}

/*void GetCurScrFont(ST_FONT *SingleCodeFont, ST_FONT *MultiCodeFont)
{	
	*SingleCodeFont = glSingleCodeFont;
	*MultiCodeFont = glMultiCodeFont;

	return;
}
*/

//display transparent string 
void  StringTransparent(int mode)
{
 
    char ele_name[100], InfoOut[100], ele_doc[1024];
    int r, len;
    char value[100];

    memset(ele_name, 0, sizeof(ele_name));
    memset(InfoOut, 0, sizeof(InfoOut));
    memset(ele_doc, 0, sizeof(ele_doc));

	strcpy(ele_name, "root"); 
    r = XmlAddElement(InfoOut, XML_DOC_MAX, ele_name, ele_doc, strlen(ele_doc), &len); 
	
    strcpy(ele_name, "StringBgColor"); 	 
	if(mode == 0 )
	{
		sprintf(ele_doc, "0");
	}
	else
	{
		sprintf(ele_doc, "1");
	}

  
	
    r = XmlAddElement(InfoOut, XML_DOC_MAX, ele_name, ele_doc, strlen(ele_doc), &len); 
	
	SysConfig(InfoOut, len);
    ScrSetFgColor(RGB(255,255,255));
	
	
}

void ScrTestProcessDisplay(int percent)
{
	int TotalPixel = 150;
	int nowPixel = 0;
	int i = 0;
	if (percent < 0 || percent > 100)
	{
		return;
	}
	nowPixel = (percent * TotalPixel)/100;
	ScrSetBgColor(RGB(46,95,177));  //gray
	ScrClrRect(125,125,125+nowPixel-1,140);
	ScrSetBgColor(RGB(194,196,200));//blue
	ScrClrRect(125+nowPixel,125,125+TotalPixel-1,140);
	ScrSetBgColor(RGB(0,0,0));
}


long GetDataFromBuffer(void* pData, uchar* pSrcData, E_DATA_TYPE eDataType)
{
	if(NULL == pSrcData || NULL == pData)
	{
		return ERR_PARAM;
	}
	if(E_DATA_TYPE_CHAR == eDataType)
	{
		*(uchar*)pData = pSrcData[0];
	}
	else if(E_DATA_TYPE_SHORT == eDataType)
	{
		*(ushort*)pData = pSrcData[0] * 256 + pSrcData[1];
	}
	else if(E_DATA_TYPE_LONG == eDataType)
	{
		*(ulong*)pData = pSrcData[0] * 16777216 + pSrcData[1] * 65536 + pSrcData[2] * 256 + pSrcData[3];
	}

	return ERR_OK;
}



void Display_Debit_Credit_PayPic(void)
{
    uchar  ucRet = 0;
    uchar  *pucSwipecard = "Please Swipe or Tap Card:";
    uint uiTextLen = 0;
    uint uiTextDispX = 0,uiTextDispY = 0;
  //  ST_FONT SingleCodeFont,MultiCodeFont;
    uint  uiTemp1 = 0;


 //   GetCurScrFont(&SingleCodeFont,&MultiCodeFont);     
    ScrCls();            
    uiTextLen = strlen(pucSwipecard);
 
    ScrAttrSet(0);
    ScrTextOut(uiTextDispX,uiTextDispY,pucSwipecard);
	

        
}


uchar DisPlay_PurChase_Amount()
{
    ushort usPos[4];
    uchar  ucFingerNum = 0;
    uchar  ucRet = 0;
    uchar  *pucPurchase = "Please Confirm the Total Purchase Amount";
    uchar  *pucNumber = "$125.34";
    uint uiTextLen = 0;
    uint uiTextDispX = 0,uiTextDispY = 0;
  //  ST_FONT SingleCodeFont,MultiCodeFont;
    uint  uiTemp1 = 0,uiTemp2 = 0;

	TimerSet(4,30 * 10);   
       
    ScrCls();            
    uiTextLen = strlen(pucPurchase);   	
   
    ScrTextOut(0,uiTextDispY,pucPurchase);    
    ScrTextOut(uiTextDispX,uiTextDispY+30,pucNumber);	
	getkey();
    
}


int Print_Receipt(uchar *logo)
{
    uchar ucRet= 0;
	uchar buff[10];
	int i = 0;
	uchar *str0 = "        PAX(SZ) Receipt\n\n";
    uchar *str1 = "STATUS  :              Approved\n";
	uchar *str2 = "MERCHANT:                   PAX\n";   
	uchar *str3 = "TYPE    :                  SALE\n";
	uchar *str4 = "ACCOUNT :   **** **** **** ****\n";
	uchar *str5 = "OPERATOR:                    01\n";
	uchar *str6 = "TERMINAL:                  NO.1\n";
	             


	memset(buff,0,sizeof(buff));
	
    PrnInit();	
	PrnStr("\n-------------------------------\n");
    PrnStr(str0);  
    PrnStr(str1);   
    PrnStr(str2);  
    PrnStr(str3);   
    PrnStr(str4);
    PrnStr(str5);
    PrnStr(str6);                      
 
	PrnStr("AMOUNT  :     %s\n",amount);
  
	
    
	if(ucRet)
	{
       ComSends("PrnStr\n");
	   return ucRet;      
	}
	
	ucRet = PrnLogo(logo);

	if(ucRet)
	{
	   ComSends("logo\n");
       return ucRet;      
	}
	
	GetTime(buff);	   
	ucRet = PrnStr("\n      20%02x/%02x/%02x %02x:%02x:%02x\n",buff[0],buff[1],buff[2],buff[3],buff[4],buff[5]);
	
	PrnStr("\nXXXX---------------------------\n");
	PrnStep(90);
	ucRet = PrnStart();
	if(ucRet)
	{
       ComSends("PrnStart %d",ucRet);
	  // return ucRet;      
	}

	 
     return ERR_OK;
	
	
}

void DisplayLogo(int iPosX,int iPosY)
{
    int iFid = 0;
    int iLen = 0;
    uchar aucBmp[500*1024];

    memset(aucBmp,0x00,sizeof(aucBmp));
    
    iFid = open("ui.bmp",O_RDWR);
	if(iFid >= 0)
	{
		iLen = read(iFid,aucBmp,sizeof(aucBmp));
    	ScrGotoxyEx(iPosX,iPosY);
    	if(iLen>0)
    	{
    		ScrDrawBitmap(aucBmp);
    	}
    	close(iFid);
	}
}




long RecvStartChar()
{
	uchar ch=0;
	int Ret=0;

	while(1)
	{   
		if(!kbhit()&&getkey()==KEYCANCEL)
	   	{
			return -1;
		}

		if (gucTerminalInfo[11]==1 && wifiStatus==1)
		{
			Ret = NetRecv(Socket, &ch, 1, 0);
			ComSends(" Ret %d ch =%02x",Ret,ch);
			if (Ret == NET_ERR_CLSD)
			{
				Socket = -1;
				return -2;
			}
			else if(Ret != 1)
			{
				continue;
			}
		}
		else
		{
			Ret = PortRecv(gucComPort, &ch, RECV_TIMEOUT);
			if(Ret)
			{
				continue;
			}
		}
		if(ch == 0x08)
		{
			break;
		}
	}
	return 0;
}


long ReceiveMsg(uchar *PackageType)
{
	ushort i = 0, lTemp = 0;
	uchar LRC = 0;
	ushort uPackageLen = 0;
	int Ret;
	ST_UPOS_MSG* pUposMsg = NULL;
	ulong s_lRecvBufLen = 0;
	ushort StartTime = 0;
	ushort EndTime = 0;
	ushort MiddleTime = 0;

	s_lRecvBufLen = 0;
	//receive package length

	memset(s_RecvBuf, 0, sizeof(s_RecvBuf));
	for(i = 0; i < 2; i++)
	{   
		if (gucTerminalInfo[11]==1 && wifiStatus==1)
		{
			TimerSet(0,100);
			while(1)
			{
				if(!kbhit()&&getkey()==KEYCANCEL)
				{
					return;
				}
				Ret = NetRecv(Socket, &s_RecvBuf[i], 1, 0);
				//ComSends("[%s,%d]i=%d,Recv=%d,0x%02x\r\n",FILELINE,i,Ret, s_RecvBuf[i]);
				if(Ret == 0)
				{
					continue;
				}
				else if (Ret == 1)
				{
					ComSends("[%s,%d]\r\n",FILELINE);
					break;
				}
				else
				{
					NetCloseSocket(Socket);
					Socket = -1;
					return ERR_UNKNOWN;
				}
				if(TimerCheck(0)==0)
				{
					break;
				}
			}
		}
		else
		{
			Ret = PortRecv(gucComPort, &s_RecvBuf[i], RECV_TIMEOUT);
			if(Ret)
			{			
				return ERR_UNKNOWN;
			}   
		}
		ComSends("%d %02x RET %d lenth%d \n",i,s_RecvBuf[i],Ret,s_lRecvBufLen);
		s_lRecvBufLen++;
	}
	

	GetDataFromBuffer(&uPackageLen, s_RecvBuf, E_DATA_TYPE_SHORT);
	if(uPackageLen > 1024)
	{
		//ScrCls();
		//ScrPrint(2, 4, 0, "Receiving data...");
	}

	// receive package 
    ScrGotoxyEx(0,50);
	ComSends("[%s,%d]uPackageLen=%d\r\n",FILELINE,uPackageLen);
	StartTime = GetTimerCount();
	TimerSet(0,200);
	
	for(i = 0; i < uPackageLen - 2; i++)
	{
		if (gucTerminalInfo[11]==1 && wifiStatus==1)
		{
			while(1)
			{
				if(!kbhit()&&getkey()==KEYCANCEL)
				{
					return;
				}
				Ret = NetRecv(Socket, s_RecvBuf + i + 2, 1, 0);
				//ComSends("[%s,%d]i=%d,Recv=%d,0x%02x\r\n",FILELINE,i,Ret, s_RecvBuf[i+2]);
				if(Ret == 0)
				{
					continue;
				}
				else if (Ret == 1)
				{
					break;
				}
				else
				{
					NetCloseSocket(Socket);
					Socket = -1;
					return ERR_UNKNOWN;
				}
				if(TimerCheck(0)==0)
				{
					break;
				}
			}
		}
		else
		{
			if(PortRecv(gucComPort, s_RecvBuf + i + 2, RECV_TIMEOUT))
			{
	  			return ERR_UNKNOWN;
			}
		}
		s_lRecvBufLen++;
	}

	EndTime = GetTimerCount();

	ComSends("Time =%ld", EndTime - StartTime);

	pUposMsg = (ST_UPOS_MSG*)s_RecvBuf;
	//check LRC
	for(i = 0; i < uPackageLen; i++)
	{
		LRC ^= s_RecvBuf[i];
	}
	//if(LRC != 0)  
	//{
	//	return ERR_LRC;
	//}
	ComSends("[%s,%d]\r\n",FILELINE);
	if(NULL == PackageType)
	{
		return ERR_PARAM;
	}
	ComSends("[%s,%d]\r\n",FILELINE);
	*PackageType = pUposMsg->PackageType;

	ComSends("package len %02x %02x ",pUposMsg->PackageSize[0],pUposMsg->PackageSize[1]);
	return ERR_OK;
}


void DisplayIP(uchar *host_ip)
{
	 ScrCls();
	 Lcdprintf("IP:");
	 ScrTextOut(0,50,host_ip);
	 getkey();
 }
void wifiListenClient();
void WifiParamConfig()
{
    uchar ucKey;
	int iRet;
	int i,j;
	uchar Resp[1024];
	uchar host_mask[64], gw_ip[64],dns_ip[64];
	ST_WIFI_PARAM WifiParam;
	ST_WIFI_AP Aps[16];
	uchar wep[5];
	int apcount;
	int Rssi;
	int Port = 60180;
	char *p;
	uchar pwd[20];
	uchar buf[20];
	uchar ucRet =0 ;
	
	memset(Resp, 0, sizeof(Resp));
	memset(&WifiParam, 0, sizeof(ST_WIFI_PARAM));
	//memset(host_ip, 0, sizeof(host_ip));
	memset(host_mask, 0, sizeof(host_mask));
	memset(gw_ip, 0, sizeof(gw_ip));
	memset(dns_ip, 0, sizeof(dns_ip));
	memset(pwd, 0 ,sizeof(pwd));
	memset(buf, 0 ,sizeof(buf));
	
	WifiParam.DhcpEnable = 1;
	

	memset(WifiParam.Wpa,0,sizeof(WifiParam.Wpa));
	memset(WifiParam.Wep,0,sizeof(WifiParam.Wep));
// 	strcpy(WifiParam.Wpa, "paxsz123");
// 	strcpy(WifiParam.Wep, "\x12\x34\x56\x78\x90");
    
	//ScrCls();
	
   // DisplayWifi();

	
	//Lcdprintf("WifiConfig %d\n",iRet );
 
	iRet = WifiScanAps(Aps,15);
	apcount = iRet;
	
	ScrCls();
	
	for (i = 0;i < apcount/9+1;i++)
	{
		if (i != (apcount/9))
		{
			for (j = 0; j<9; j++)
			{
				ComSends("No.%d\r\n",j+i*9);
				ComSends("Ssid=%s\r\n",Aps[j+i*9].Ssid);
				ComSends("Rssi=%d\r\n",Aps[j+i*9].Rssi);
				ComSends("SecMode=%d\r\n",Aps[j+i*9].SecMode);
				Lcdprintf("No.%d Ssid=%s\n",j,Aps[j+i*9].Ssid);
			}
		}
		else
		{
			for (j = 0; j<apcount%9; j++)
			{
				ComSends("No.%d\r\n",j+i*9);
				ComSends("Ssid=%s\r\n",Aps[j+i*9].Ssid);
				ComSends("Rssi=%d\r\n",Aps[j+i*9].Rssi);
				ComSends("SecMode=%d\r\n",Aps[j+i*9].SecMode);
				Lcdprintf("No.%d Ssid=%s\n",j,Aps[j+i*9].Ssid);
			}
		}
		kbflush();
		ucKey= getkey();

		if((ucKey ==KEYF2) || (ucKey ==KEYF1)||(ucKey ==KEYCLEAR)||(ucKey ==KEYENTER))
		{
			ScrCls();
			if (i == apcount/9)
			{

				i = 0;
			}
			continue;
		}
		else if(ucKey ==KEYCANCEL)

		{
		 	return;
		}
		else
		{
			ucKey = ucKey+i*9;
			break;
		 }
	}
	  
	ScrTestProcessDisplay(40);

	kbflush(); 
	DispPassWordInput();
	ScrSetFgColor(RGB(0,0,0));
    //	StringTransparent();
	ScrGotoxyEx(135,125);	
	ucRet = GetString(buf,0xf5,5,10);//length is 6;input the password
	ScrSetBgColor(RGB(0,0,0));
	ScrSetFgColor(RGB(255,255,255));
	StringTransparent(0);   
	if(ucRet != 0)
	{
		return ;
	}

  	if (Aps[ucKey-'0'].SecMode == WLAN_SEC_WEP)
	{

		ComSends("SecMode=%02d  WLAN_SEC_WEP\r\n",Aps[ucKey-'0'].SecMode);

		if (buf[0] == 5)
		{
			memcpy(WifiParam.Wep,buf+1,buf[0]);
		}
		else if (buf[0] == 10)
		{
			for (i = 0;i < 5;i++)
			{
				if (s_StringToHex(buf+1+2*i, wep+i))
				{
					return;
				}
			}
			memcpy(WifiParam.Wep,wep,sizeof(wep));
		}
	}
	else
	{
		ComSends("SecMode=%02d  WLAN_SEC_WPA\r\n",Aps[ucKey-'0'].SecMode);

		memcpy(WifiParam.Wpa,buf+1,buf[0]);
	}

	ComSends("wep=%02x%02x%02x%02x%02x\r\n",WifiParam.Wep[0],WifiParam.Wep[1],
		WifiParam.Wep[2],WifiParam.Wep[3],WifiParam.Wep[4]);

	ComSends("wpa=%s\r\n",WifiParam.Wpa);

	memcpy(&glWifiParam, &WifiParam, sizeof(ST_WIFI_PARAM));
	memcpy(&glApParam, &Aps[ucKey-'0'], sizeof(ST_WIFI_AP));

	ScrCls();
	DisplayWifi();
      
	ScrTestProcessDisplay(60);

	iRet = WifiConnectAp(&Aps[ucKey-'0'], &WifiParam);
	ScrTestProcessDisplay(65);

	//ScrCls();
	ComSends("Connect%s %d",Aps[ucKey-'0'].Ssid,iRet );
	    
	if(iRet!=0)
	{
		ScrCls();
		DisplayBang();
		getkey();
		return ;
	}
	while (1) 
 	{ 
	 	iRet = WifiCheck(NULL); 
 		if (iRet > 0) 
 		{ 
 			break; 
	 	} 
 		else if(iRet == 0) 
 		{ 
 			continue; 
 		} 
		else
		{
			ScrCls();
			Lcdprintf("Wifi Connect Fail!");
			getkey();
			return;
		}
 	}
	ScrTestProcessDisplay(80);
	iRet = 	NetDevGet(WIFI_ROUTE_NUM, host_ip, host_mask, gw_ip, dns_ip);
	ScrTestProcessDisplay(85);
	wifiListenClient();
	ScrTestProcessDisplay(100);
    ComSends("iRet=%d ip=%s,mask=%s,gate=%s,dns=%s\r\n",iRet,host_ip, host_mask, gw_ip, dns_ip);
    DisplayIP(host_ip);

}

void wifiListenClient()
{
	int Port;
	uchar ClientIp[40];
	int ClientPort= 0;
	int iRet = 0;
	struct net_sockaddr Listenaddr, Clientaddr;

	memset(ClientIp,0,strlen(ClientIp));
  
	Port = 60180;

	if(Socket > 0)
	{
		NetCloseSocket(Socket);
		Socket = -1;
	}
	if (listenSocket >= 0)
	{
		NetCloseSocket(listenSocket);
		listenSocket = -1;
	}
	iRet = SockAddrSet(&Listenaddr, "0.0.0.0", Port);
	ComSends("[%s,%d]SockAddrSet=%d\r\n",FILELINE,iRet);
	listenSocket = NetSocket(NET_AF_INET, NET_SOCK_STREAM, 0);
	ComSends("[%s,%d]NetSocket=%d\r\n",FILELINE,listenSocket);
	iRet = NetBind(listenSocket, &Listenaddr, sizeof(struct net_sockaddr));
	ComSends("[%s,%d]NetBind=%d\r\n",FILELINE,iRet);
	iRet = NetListen(listenSocket, 1);
	ComSends("[%s,%d]NetListen=%d\r\n",FILELINE,iRet);
	Netioctl(listenSocket, CMD_IO_SET, NOBLOCK);
	ComSends("listenSocket %d",listenSocket);

}
int WifiAcceptClient(void)
{
	struct net_sockaddr Clientaddr;
	socklen_t addrlen;
	if (listenSocket < 0)
	{
		wifiListenClient();
	}
	if(listenSocket >= 0)
	{
   	   
		while(1)
	   	{
			if(!kbhit()&&getkey()==KEYCANCEL)
			{
				return -1;
			}
			if (Netioctl(listenSocket, CMD_EVENT_GET, SOCK_EVENT_ACCEPT))
			{
				Socket = NetAccept(listenSocket,&Clientaddr,&addrlen);
				ComSends("Socket %d",Socket);
		   		if(Socket>0)
		   		{
		   			Netioctl(Socket, CMD_IO_SET, NOBLOCK);
					NetCloseSocket(listenSocket);
					listenSocket = -1;
					break;
				}
				DelayMs(100);
			}
		}
		return 0;
	}
	return -2;
}

void InitWifiParam()
{
	memset(&glWifiParam, 0, sizeof(glWifiParam));
	memset(&glApParam, 0, sizeof(glApParam));
}

void WifiRelogin()
{
	int iRet;
	
	while(1)
	{
		WifiClose(); 
	 	WifiOpen(); 
		
		iRet = WifiConnectAp(&glApParam, &glWifiParam); 
	 	if(iRet) 
	 	{ 
			continue; 
		} 

	 	while (1) 
	 	{ 
		 	iRet = WifiCheck(NULL); 
	 		if (iRet > 0) 
	 		{ 
	 			return; 
		 	} 
	 		else if(iRet == 0) 
	 		{ 
	 			continue; 
	 		} 
			break;
	 	}
	}
}

long OpenWifiPort()
{
	long Ret ; 
	int ucRet = 0;
	int i = 0;

	InitWifiParam();

	ScrCls();
    	DisplayWifi();
	ScrTestProcessDisplay(10);

    //DisplayWifi();
    Ret = WifiOpen();   	
		  
	if(Ret != 0)
	{
		Beep();
		ScrCls();		
		Lcdprintf("fail!ret %d", Ret);
		getkey();
		return Ret;
	}
	else
	{
		WFflag = 1;// wifi has opened
		ScrGotoxyEx(0,50);
	
		ScrTestProcessDisplay(20);
		WifiParamConfig();//config the parameter
	}

	return 0;
}

void BTListen()
{
	uchar BtStatus[7];
	long Ret ; 
	int ucRet;

	ScrCls();

	if(gucComPort == 7)
	{
	
		TimerSet(0,20*10);	  
		while(1)
		{
         
			BtGetStatus(BtStatus);
			ScrGotoxyEx(0,100);
			// Lcdprintf("state %d ",BtStatus.state);
			if(BtStatus[0]==1)
			{
				break;
			}

			if(TimerCheck(0)==0)
			{
				Lcdprintf("BT Connect Error");
				getkey();
				break ;
			}
		}
		DelayMs(200);
		PortReset(gucComPort); 	  
	}
}


long OpenBTPort()
{  
	long Ret ; 
	int ucRet;
	int i=0;
	uchar buf[20];
	uchar strname[20];

	memset(buf,0,20);
	memset(strname,0,20);
	
	DisplayBT();	
	for(i=0;i<20;i++)
	{
		ScrTestProcessDisplay(i);
	}
	

	if(gucComPort == 7)
	{
		if(BTflag == 0)
		{
			Ret = BtOpen();		  
			ComSends("Btopen %d",Ret);
		}
		else
		{
			BtClose();
			Ret = BtOpen();		  
			ComSends("Btopen %d",Ret);
		}  
	}
	else
	{ 
		strcpy(PORTOPEN_PARA,"115200,8,n,1");
		Ret = PortOpen(gucComPort, PORTOPEN_PARA);
	}
	
   	if(Ret != 0)
	{
		Beep();	
		return Ret;
	}
	else
	{
		BTflag = 1;	
		for (i = 20;i < 70;i++)
		{
			ScrTestProcessDisplay(i);
		}
		ucRet = BtSetName("BTMYTEST2");		
		//ucRet = BtSetName(strname);
        ComSends("BtSetName %d  %s\n",ucRet,strname);
		if(ucRet!=0)
		{
			return Ret;
		}
        ucRet = BtSetPin("1234");
        ComSends("BtPin %d \n",ucRet);
		if(ucRet!=0)
		{
			return Ret;
		}
		ucRet = BtSetRole(0);  //set the slave mode 
		ComSends("Role %d \n",ucRet);
		if(ucRet!=0)
		{
			return Ret;
		}
	}
	return 0;
}


long PutDatatoBuffer(uchar* pDestBuffer, ulong lData, E_DATA_TYPE eDataType)
{
	if(NULL == pDestBuffer)
	{
		return ERR_PARAM;
	}
	if(E_DATA_TYPE_SHORT == eDataType)
	{
		pDestBuffer[0] = lData / 256;
		pDestBuffer[1] = lData % 256;
	}
	else if(E_DATA_TYPE_LONG == eDataType)
	{
		pDestBuffer[0] = lData / 16777216;
		pDestBuffer[1] = (lData / 65536) % 256;
		pDestBuffer[2] = (lData / 256) % 256;
		pDestBuffer[3] = lData % 256;
	}
	else if(E_DATA_TYPE_CHAR)
	{
		pDestBuffer[0] = lData % 256;
	}

	return ERR_OK;
}


///////////////////////////////////////
/// @brief: send response to pc
/// @return:
/// @note:
/// @see:
///////////////////////////////////////
long SendResponse(uchar* pSendData, long lSendLen)
{
	//compute LRC
	ulong i = 0;
	uchar LRC = 0;
	int Ret = ERR_UNKNOWN;

	if(lSendLen <= 0 || NULL == pSendData)
	{
		return ERR_PARAM;
	}
	for(i = 1; i < lSendLen - 1; i++)
	{
		LRC ^= pSendData[i];
	}
	memcpy(pSendData + lSendLen - 1, &LRC, sizeof(uchar));

	// send response
	for(i = 0; i < 3; i++)
	{
  		if (gucTerminalInfo[11]==1 && wifiStatus==1)
  		{
			if(NetSend(Socket , pSendData,(ushort)lSendLen, 0)>0)
			{
				ComSends("[%s,%d]\r\n",FILELINE);
				Ret = ERR_OK;
				break;
			}
  		}
 		else
 		{
	        if(!PortSends(gucComPort, pSendData, (ushort)lSendLen))
			{
				Ret = ERR_OK;
				break;
			}
 		}	
	}

	return Ret;
}

///////////////////////////////////////
/// @brief: Recieve message of amount
/// @return:amount 
/// @note:
/// @see:
///////////////////////////////////////

int  MagMessage(uchar *amount)//recieve the transaction amount,amount as output
{
	uchar APIRet = 0 ;
	ST_UPOS_MSG  pMsg={0};
	ushort ErrorCode = ERR_OK;
	ushort PackageSize ; 
	uchar StartChar = 0x08;
	long lError;
	uchar PackageType;
	int Param_len;
	int i;

   
	if (amount== NULL)
	{
		return  2;// null 
	}

	//TimerSet(0,1000);//set timers

	while(1)
	{
	   
		i= RecvStartChar(); 
		if (i == -1) return -1;
		if (i == -2) return -2;
		lError = ReceiveMsg(&PackageType);	
		ComSends("[%s,%d]lError=%d,Type=%d\r\n",FILELINE,lError,PackageType);
		if(PackageType!=1)  //correct Package
		{
			return 1;      //error package
			// continue;
		}	
		if(ERR_OK != lError)
		{
			APIRet = PortReset(gucComPort);
			continue;
		}
		else
		{
			break;
		}
	}

	Param_len = (s_RecvBuf[3] << 8) | (s_RecvBuf[4]);
	memset(amount,0x00,Param_len+1);
	memcpy(amount,s_RecvBuf+5,Param_len); 
	
	memcpy(s_SendBuf, &StartChar, sizeof(uchar));//add the package head
	

	pMsg.PackageType = 0x01;  		                                                                               
    PackageSize= 0x05;		
	
    PutDatatoBuffer(pMsg.PackageSize, PackageSize, E_DATA_TYPE_SHORT);
    memcpy(s_SendBuf + 1, &pMsg, sizeof(pMsg));
	
	s_SendBuf[4] = lError;
	s_lSendBufLen = 6;
	
    return SendResponse(s_SendBuf, s_lSendBufLen);

}

int SendPrintString(uchar *String,int ErrorCode)//if failed,it need 
{
	ST_UPOS_MSG  pMsg={0};
	ushort PackageSize ;
	uchar StartChar = 0x08; 
	uchar APIRet;
	uchar PackageType;
	long lError;
	int i;
   
	memset(s_RecvBuf,0x00,sizeof(s_RecvBuf));   
	memcpy(s_SendBuf, &StartChar, sizeof(uchar));//add the package head
	pMsg.PackageType = 0x02;//print package 		
   
  
	if(String == NULL)
   	{
          return 2;
    }
	if(ErrorCode==0)
	{
		s_SendBuf[4] = 0;
		s_SendBuf[5] = 0;
		s_SendBuf[6] = strlen(String)/256;
		s_SendBuf[7] = strlen(String)%256;
		memcpy(s_SendBuf+8,String,strlen(String));
		s_lSendBufLen= 8 + strlen(String);
	}
	else
	{
		s_SendBuf[4] = ErrorCode / 256;
		s_SendBuf[5] = ErrorCode % 256;
		s_lSendBufLen = 6;  
	}
	PutDatatoBuffer(pMsg.PackageSize, s_lSendBufLen, E_DATA_TYPE_SHORT);
	memcpy(s_SendBuf + 1, &pMsg, sizeof(pMsg));
	SendResponse(s_SendBuf, s_lSendBufLen+1);
	PortSends(0,s_SendBuf,s_lSendBufLen+1);
	while(1)
	{
		i= RecvStartChar(); 	 
		if (i == -1) return -1;
		if (i == -2) return -2;
		lError = ReceiveMsg(&PackageType);	
		if(PackageType!=2)  //correct Package
		{
			return 1;//error package
			//continue;
		}
				   
		if(ERR_OK != lError)
		{
			APIRet = PortReset(gucComPort);
			continue;
		}
		else
		{
			break; 
		}
	}


	if(s_RecvBuf[3]!=0)//judge error code ,
	{
		return ERR_FAIL;
	}
	else 
	{
		return ERR_OK;
	}
}

void Receipt(uchar *string)//print receipt
{
	uchar *str_0 ="\n-------------------------------\n";
	uchar *str0 = "        PAX(SZ) Receipt\n\n";
    uchar *str1 = "STATUS  :              Approved\n";
	uchar *str2 = "MERCHANT:                   PAX\n";   
	uchar *str3 = "TYPE    :                  SALE\n";
	uchar *str4 = "ACCOUNT :   **** **** **** ****\n";
	uchar *str5 = "OPERATOR:                    01\n";
	uchar *str6 = "TERMINAL:                  NO.1\n";
	uchar *str7 = "AMOUNT  :       ";
	uchar *str8 ="\nXXXX---------------------------\n";
	

	
	uchar str[1024];

	memset(str,0x00,1024);
	
	strcpy(str,str_0);
	strcat(str,str0 );
	ComSends("strcpy0\n");
	strcat(str,str1);
	ComSends("strcpy1\n");
	strcat(str,str2);
	ComSends("strcpy2\n");
	strcat(str,str3);
	ComSends("strcpy3\n");
	strcat(str,str4);
	ComSends("strcpy4\n");
	strcat(str,str5);
	ComSends("strcpy5\n");
	strcat(str,str6);
	ComSends("strcpy6\n");
	strcat(str,str7);
	
	strcat(str,amount);
	strcat(str,str8);

	
	strcpy(string,str);
	//ScrCls();
	ComSends("strcpy\n");
	  

/* 字符串中的空格是为了后边的有效期而设的 */

}


//cancel 取消时发送包

int CancelPackage()
{

	ST_UPOS_MSG  pMsg={0};
	ushort PackageSize ;
	uchar StartChar = 0x08; 
	uchar APIRet;
	uchar PackageType;
	long lError;
	int i;

	memcpy(s_SendBuf, &StartChar, sizeof(uchar));//add the package head
	pMsg.PackageType = 0x02;//cancel  package 


	s_SendBuf[4] = 0;
	s_SendBuf[5] = 0xff;
	s_SendBuf[6] = 0;
	s_SendBuf[7] = 0;

	PackageSize=0x03; 
	s_lSendBufLen =9;

	PutDatatoBuffer(pMsg.PackageSize, PackageSize, E_DATA_TYPE_SHORT);
	memcpy(s_SendBuf + 1, &pMsg, sizeof(pMsg));  

	SendResponse(s_SendBuf, s_lSendBufLen);


}


int PrintReceipt(uchar *logo)//receive the receipt 
{
	ST_UPOS_MSG  pMsg={0};
	ushort PackageSize ;
	uchar StartChar = 0x08; 
	uchar APIRet;
	uchar PackageType;
	long lError;
	int i;
	long Param_len;

	while(1)
	{	   
		i= RecvStartChar(); 	
		if (i == -1) return -1;
		if (i == -2) return -2;
		lError = ReceiveMsg(&PackageType); 
		if(PackageType!=3)  //correct Package
		{
			return 1;//error package
			//continue;
		}
					  
		if(ERR_OK != lError)
		{
			APIRet = PortReset(gucComPort);
			continue;
		}
		else
		{
			break;				
		}
   
	}


	Param_len = (s_RecvBuf[3] << 8) | (s_RecvBuf[4]);
	memcpy(logo,s_RecvBuf+5,Param_len); 
	ComSends(" logo lenth:%d",Param_len);
	#ifdef _DEBUG_
	/*for(i=0;i<Param_len;i++)
	{
		ComSends("%d=%02x",i,s_RecvBuf[5+i]);
		DelayMs(20);
	}
	*/
	#endif
	memcpy(s_SendBuf, &StartChar, sizeof(uchar));//add the package head 

	pMsg.PackageType = 0x03;																						
	PackageSize= 0x05; 
	 
	s_SendBuf[4] = lError;
	s_lSendBufLen = 6;
	 
	PutDatatoBuffer(pMsg.PackageSize, PackageSize, E_DATA_TYPE_SHORT);
	memcpy(s_SendBuf + 1, &pMsg, sizeof(pMsg));
	  	
	 
	SendResponse(s_SendBuf, s_lSendBufLen);
	// ComSends("%s ",s_SendBuf);
	   
}


int TradeResponse(int ErrorCode)//transaction error code 
{
    
	ST_UPOS_MSG  pMsg={0};
	ushort PackageSize ;
	uchar StartChar = 0x08; 
	uchar APIRet;
	uchar PackageType;
	long lError;
	int i;

	memcpy(s_SendBuf, &StartChar, sizeof(uchar));//add the package head
	pMsg.PackageType = 0x04;//print package	
  
	s_SendBuf[4] = 0x00; 
	s_SendBuf[5] = 0x01;
	s_SendBuf[6] = ErrorCode;
	
	PackageSize=0x04; 
	s_lSendBufLen =8;

	PutDatatoBuffer(pMsg.PackageSize, PackageSize, E_DATA_TYPE_SHORT);
	memcpy(s_SendBuf + 1, &pMsg, sizeof(pMsg));  
  
	SendResponse(s_SendBuf, s_lSendBufLen);



	while(1)
	{
	
		i= RecvStartChar(); 	
		if (i == -1) return -1;
		if (i == -2) return -2;
		lError = ReceiveMsg(&PackageType);	
		if(PackageType != 4)  //correct Package
		{
			return 1;//error package
			//continue;
		}
				   
		if(ERR_OK != lError)
		{
			APIRet = PortReset(gucComPort);
			continue;
		}
		else
		{
			break; 
		}
	}
   
	if(s_RecvBuf[3]==0)//judge error code ,
	{
		return ERR_OK;
	}
	else 
	{
		return ERR_FAIL;
	}
}


uchar LoadingPng()
{
	uchar ucRet;   

    ScrProcessImage("swipe.png",tiftPng,tictLoad,0,0); 
	ScrProcessImage("waiting.png",tiftPng,tictLoad,0,0); 
	ScrProcessImage("amount.png",tiftPng,tictLoad,0,0); 
	ScrProcessImage("printing.png",tiftPng,tictLoad,0,0); 
	ScrProcessImage("card.png",tiftPng,tictLoad,0,0); 
	ScrProcessImage("bang.png",tiftPng,tictLoad,0,0); 
	ScrProcessImage("failed.png",tiftPng,tictLoad,0,0); 
	ScrProcessImage("ok.png",tiftPng,tictLoad,0,0); 
	ScrProcessImage("colixo.png",tiftPng,tictLoad,0,0); // Colixo logo
	ScrProcessImage("trading.png",tiftPng,tictLoad,0,0); // trading 
	ScrProcessImage("wifi.png",tiftPng,tictLoad,0,0); //wifi 
	ScrProcessImage("BT.png",tiftPng,tictLoad,0,0);    //BT
	ScrProcessImage("TimeSet.png",tiftPng,tictLoad,0,0); //set time
	ScrProcessImage("modifypw.png",tiftPng,tictLoad,0,0); //modify
	ScrProcessImage("password.png",tiftPng,tictLoad,0,0);   //input password 
	ScrProcessImage("btpro.png",tiftPng,tictLoad,0,0);
	ScrProcessImage("wifipro.png",tiftPng,tictLoad,0,0);
	ucRet = ScrProcessImage("upload.png",tiftPng,tictLoad,0,0); //upload
    
	if(ucRet == 0) 
	{
		ucRet = open("LoadPic",O_CREATE);
		return ucRet; 
	}
}


void DisplayAPPMenu_down()
{ 
     ScrClrRect(0,0,319,239); 	 
	 ScrProcessImage("TimeSet.png",tiftPng,tictDisplay,64,11); //modify
	 ScrProcessImage("upload.png",tiftPng,tictDisplay,164,11); //upload

}

void DisplayAPPMenu_up()
{
     ScrClrRect(0,0,319,239); 	 
	 ScrProcessImage("trading.png",tiftPng,tictDisplay,64,11); // trading 
 	 ScrProcessImage("wifi.png",tiftPng,tictDisplay,164,11); //wifi 
 	 ScrProcessImage("BT.png",tiftPng,tictDisplay,64,111);    //BT
 	 ScrProcessImage("modifypw.png",tiftPng,tictDisplay,164,111); //set time

}


void DisplayMenu(uchar page)
{
	if(  page== 0)
   	{
		DisplayAPPMenu_up();
   	}
	else if(page == 1)   	
   	{
     	DisplayAPPMenu_down();
   	}

	
}


//显示待机界面
void Dispsuspend()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "衔评擞裳依 颇纫?");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "Please Waiting...");
		strlen1 = strlen(str1);
	}
	ScrCls();
	ScrProcessImage("swipe.png",tiftPng,tictDisplay,30,76);
	ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);
}

//显示待机界面
void DisplayMagswipe()
{    
	char str1[256];
	char str2[256];
	char str3[256];
	int strlen1,strlen2,strlen3;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	memset(str3, 0, sizeof(str3));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "???????");
		strlen1 = strlen(str1);
		strcpy(str2, "???...");
		strlen2 = strlen(str2);
	}
	else
	{
		strcpy(str1, "Please Swipe/");
		strlen1 = strlen(str1);
		strcpy(str2, "Tap/Insert");
		strlen2 = strlen(str2);
		strcpy(str3, "Card...");
		strlen3 = strlen(str3);
	}
	ScrCls();
	ScrProcessImage("waiting.png",tiftPng,tictDisplay,30,76); 
	if (CheckLanguage()==2)
	{
		ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);
		ScrTextOut(100+((320-100-strlen2*12)/2),120,str2);
	}
	else
	{
		ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);
		ScrTextOut(100+((320-100-strlen2*12)/2),120,str2);
		ScrTextOut(100+((320-100-strlen3*12)/2),150,str3);
	}
}


//显示主页
void DisplayMainPage()
{
	ScrProcessImage("main.png",tiftPng,tictDisplay,0,0); 
}

//显示或输入金额
void DisplayAmount()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "衔囊屡心纫?延烫?");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "Please Confirm the Amount");
		strlen1 = strlen(str1);
	}
	ScrCls();
	ScrProcessImage("amount.png",tiftPng,tictDisplay,30,76);	  
	ScrTextOut((320-strlen1*12)/2,170,str1);
}

//显示交易进行中
void DisplayTrading()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "瘟膛?睦屯厶?");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "Transaction is ");
		strlen1 = strlen(str1);
		strcpy(str2, "in progress...");
		strlen2 = strlen(str2);
	}
	ScrCls();
    ScrProcessImage("waiting.png",tiftPng,tictDisplay,30,76);    
	ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);
	ScrTextOut(100+((320-100-strlen2*12)/2),120,str2);
}
//交易完成
void DispOverTrading()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "蜗判乐冗 文瘟信屠");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "Transaction is");
		strlen1 = strlen(str1);
		strcpy(str2, "OK!");
		strlen2 = strlen(str2);
	}
	ScrCls();
	ScrProcessImage("ok.png",tiftPng,tictDisplay,30,76);
	ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);
	ScrTextOut(100+((320-100-strlen2*12)/2),120,str2);
}
//交易失败
void DispTradingFail()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "蜗判乐冗 我仕瓮磐?");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "Transaction is ");
		strlen1 = strlen(str1);
		strcpy(str2, "Fail!");
		strlen2 = strlen(str2);
	}
	ScrCls();
    ScrProcessImage("failed.png",tiftPng,tictDisplay,30,76);
    ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);
	ScrTextOut(100+((320-100-strlen2*12)/2),120,str2);
}

void DisplayBang()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "呜攘世 崖咔?");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "Connection Error!");
		strlen1 = strlen(str1);
	}
	ScrCls();
    ScrProcessImage("bang.png",tiftPng,tictDisplay,30,76);
	ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);

}

//账单打印中
void DispReceiptPrinting()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "吓桌依?着?..");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "Receipt is ");
		strlen1 = strlen(str1);
		strcpy(str2, "Printing...");
		strlen2 = strlen(str2);
	}
	ScrCls();
    ScrProcessImage("printing.png",tiftPng,tictDisplay,50,76);
	ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);
	ScrTextOut(100+((320-100-strlen2*12)/2),120,str2);
}

void DisplayWifi()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "盐拍韧咿衍...");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "connecting");
		strlen1 = strlen(str1);
	}
	ScrCls();
	ScrProcessImage("wifipro.png",tiftPng,tictDisplay,30,76);
	ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);

}

void DisplayBT()
{
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "盐拍韧咿衍...");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "connecting");
		strlen1 = strlen(str1);
	}
	ScrCls();
	ScrProcessImage("btpro.png",tiftPng,tictDisplay,30,76);
	ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);

}


void DispPassWordInput()
{
	int i = 0;
	char str1[256];
	char str2[256];
	int strlen1,strlen2;
	memset(str1, 0, sizeof(str1));
	memset(str2, 0, sizeof(str2));
	if (CheckLanguage()==2)
	{
		strcpy(str1, "侣拍纫?先?");
		strlen1 = strlen(str1);
	}
	else
	{
		strcpy(str1, "Input the PWD");
		strlen1 = strlen(str1);
	}
	ScrCls();
	ScrProcessImage("amount.png",tiftPng,tictDisplay,30,76);
	ScrTextOut(115,76,str1);

	ScrSetBgColor(RGB(255,255,255));
	ScrClrRect(115,120,270,151);
	ScrSetFgColor(RGB(0,0,0));
}



void StartPage()
{
   
	ScrClrRect(0,0,320,240); 

	ScrProcessImage("002.png",tiftPng,tictDisplay,172,58);//select frame
	ScrProcessImage("menu_app.png",tiftPng,tictDisplay,63,67);//app menu
	ScrProcessImage("manage_iron.png",tiftPng,tictDisplay,172,67);//manage iron   
	ScrProcessImage("006.png",tiftPng,tictDisplay,62,144);    //button
	ScrTextOut(68,146," App");
	ScrProcessImage("006.png",tiftPng,tictDisplay,178,144);  
	ScrTextOut(180,146," Tool");
}

void SetTimeDisplay()
{
	ScrClrRect(0,24,320,240); 
	ScrProcessImage("time_menu.png",tiftPng,tictDisplay,56,24);//set time menu 
	ScrProcessImage("year.png",tiftPng,tictDisplay,22,80);// year
	ScrTextOut(120,85," Y "); 
	ScrProcessImage("other.png",tiftPng,tictDisplay,147,80);//month
	ScrTextOut(197,85," M ");
	ScrProcessImage("other.png",tiftPng,tictDisplay,228,80);// day
	ScrTextOut(271,85," D ");
	ScrProcessImage("other.png",tiftPng,tictDisplay,22,131);//hour 
	ScrTextOut(76,140,"H ");
	ScrProcessImage("other.png",tiftPng,tictDisplay,100,131);// Minute
	ScrTextOut(160,140,"M ");
	ScrProcessImage("007.png",tiftPng,tictDisplay,72,193);//CANCEL
	ScrTextOut(80,203,"CANCEL");
	ScrProcessImage("009.png",tiftPng,tictDisplay,164,193);//ENTER
	ScrTextOut(175,203,"ENTER"); 

}

//modify
void ModifyPassword()
{

	ScrClrRect(0,0,320,240); 
  
	ScrProcessImage("modify_pw.png",tiftPng,tictDisplay,56,24);// input password   menu
	ScrTextOut(15,76,"OLD PWD");
	ScrProcessImage("password.png",tiftPng,tictDisplay,106,70);// password 
	ScrTextOut(15,120,"NEW PWD");
	ScrProcessImage("password.png",tiftPng,tictDisplay,106,111);// password1 
	ScrTextOut(10,162,"Confirm ");
	ScrProcessImage("password.png",tiftPng,tictDisplay,106,152);// password2 
  
	ScrProcessImage("007.png",tiftPng,tictDisplay,72,193);//CANCEL
	ScrTextOut(80,203,"CANCEL");
	ScrProcessImage("009.png",tiftPng,tictDisplay,164,193);//ENTER
	ScrTextOut(175,203,"ENTER"); 
}


void DisplayItem(int flush_flag,int FromItem,int ToItem)
{
	int backFgColor;
	if (flush_flag)
	{
		ScrCls();
		/**
		ScrProcessImage("trading.png",tiftPng,tictDisplay,
			10,12); 
		ScrProcessImage("wifi.png",tiftPng,tictDisplay,
			10+10+145,12);
		ScrProcessImage("BT.png",tiftPng,tictDisplay,
			10,12+12+90);
		ScrProcessImage("modifypw.png",tiftPng,tictDisplay,
			10+10+145,12+12+90);
		**/
		ScrProcessImage("colixo.png",tiftPng,tictDisplay,
			70,80); 
		//ScrProcessImage("wifi.png",tiftPng,tictDisplay,
		//	10+10+145,12);
		//ScrProcessImage("trading.png",tiftPng,tictDisplay,
		//	10,12+12+90);
		//ScrProcessImage("modifypw.png",tiftPng,tictDisplay,
		//	10+10+145,12+12+90);
	}
	if (FromItem == 0)
	{
		ScrSetFgColor(RGB(0,0,0));
		ScrDrawRect(8,10,156,103);
		ScrDrawRect(9,11,155,102);
		ScrSetFgColor(RGB(194,196,200));
	}
	else if (FromItem == 1)
	{
		ScrSetFgColor(RGB(0,0,0));
		ScrDrawRect(163,10,311,103);
		ScrDrawRect(164,11,310,102);
		ScrSetFgColor(RGB(194,196,200));
	}
	else if (FromItem == 2)
	{
		ScrSetFgColor(RGB(0,0,0));
		ScrDrawRect(8,112,156,205);
		ScrDrawRect(9,113,155,204);
		ScrSetFgColor(RGB(194,196,200));
	}
	else if (FromItem == 3)
	{
		ScrSetFgColor(RGB(0,0,0));
		ScrDrawRect(163,112,311,205);
		ScrDrawRect(164,113,310,204);
		ScrSetFgColor(RGB(194,196,200));
	}

	if (ToItem == 0)
	{
		ScrSetFgColor(RGB(194,196,200));
		ScrDrawRect(8,10,156,103);
		ScrDrawRect(9,11,155,102);
	}
	else if (ToItem == 1)
	{
		ScrSetFgColor(RGB(194,196,200));
		ScrDrawRect(163,10,311,103);
		ScrDrawRect(164,11,310,102);
	}
	else if (ToItem == 2)
	{
		ScrSetFgColor(RGB(194,196,200));
		ScrDrawRect(8,112,156,205);
		ScrDrawRect(9,113,155,204);
	}
	else if (ToItem == 3)
	{
		ScrSetFgColor(RGB(194,196,200));
		ScrDrawRect(163,112,311,205);
		ScrDrawRect(164,113,310,204);
	}
}

void ConnectWIFI(int select_bak, int select)
{
	int i;
	
	ComSends("connect wifi");
	if (gucTerminalInfo[11] == 1)
	{
		gucComPort = 5;
		if(WFflag==0)
		{
			OpenWifiPort();
		}
		else
		{
			ScrCls();
			DisplayWifi();
			for (i=0;i<50;i++)
			{
				ScrTestProcessDisplay(i);
			}
			wifiListenClient();
			for (i=50;i<100;i++)
			{
				ScrTestProcessDisplay(i);
			}
			DisplayIP(host_ip);
		}
		ComSends("OPEN COM");
		DisplayItem(1, -1, select);
	}
	else
	{
		if (select != select_bak)
		{
			DisplayItem(0, select_bak, select);
		}
	}
}

void DisconnectWIFI()
{
	int iRet;
	
	ComSends("disconnect wifi");
	ScrCls();
	ScrGotoxyEx(0, 24);
	iRet = WifiDisconAp();
	if(iRet)
	{
		Lcdprintf("Disconnect error: %d", iRet);
	}
	else
	{
		Lcdprintf("Disconnect success");
		WFflag = 0;
	}
	getkey();
}

void EnterItem(int select_bak,int select)
{
	int iRet;
	
	if (select == 0)
	{
		ComSends("trade");
		//Credit_Trade();  /*swipe app*/
		WaitingStateEngine() ;

		DisplayItem(1, -1, select);
	}
	else if (select == 1)
	{
		ComSends("wifi");
		ScrCls();
		Lcdprintf("WIFI SETUP");
		ScrGotoxyEx(0, 24);
		Lcdprintf("1. Connect WIFI\n");
		Lcdprintf("2. Disconnect WIFI");

		iRet = getkey();
		if(KEY1 == iRet)
		{
			ConnectWIFI(select_bak, select);
		}
		else if(KEY2 == iRet)
		{
			DisconnectWIFI();
		}
	}
	else if (select == 2)
	{
		if (gucTerminalInfo[20] == 1)
		{
			gucComPort = 7;
			iRet = OpenBTPort();
			if(iRet!=0)
			{
				DisplayBang();
				getkey();
			}
			DisplayItem(1, -1, select);
		}
		else
		{
			if (select != select_bak)
			{
				DisplayItem(0, select_bak, select);
			}
		}
	}
	else if (select == 3)
	{
		/* modify*/
		if (select != select_bak)
		{
			DisplayItem(0, select_bak, select);
		}
	}
	else
	{
		return;
	}
	
}
void DisplayAPPMenu()
{
	uchar ch;
	int select =0 ; 
	int select_bak=0;
	int page =0;
	int flag =0;
	int ucRet =0;
	int i;

	ComSends("APP");

	
	DisplayItem(1,-1,0);
	ScrSetFgColor(RGB(194,196,200));
	select = 0;

	if (gucTerminalInfo[20] == 1)
	{
		gucComPort = 7;

		if(OpenBTPort()!=0)
		{
			DisplayBang();
			getkey();
		}
		DisplayItem(1, -1, select);
	}
	while(1)
	{
		WaitingStateEngine() ;

		if(WFflag)
		{
			if(WifiCheck(NULL) < 0)
			{
				WifiRelogin();
			}
		}
	}
#if 0
	while(1)
   	{
		if(kbhit()==0x00)
		{
		   	ch = getkey();
			switch(ch)
			{
				case KEYF1:
					select_bak = select;
					select = (select + 1)%4;
					DisplayItem(0,select_bak,select);
					break;
				case KEYF2:
					select_bak = select;
					select = (select + 4 - 1)%4;
					DisplayItem(0,select_bak,select);
					break;
				case KEYCANCEL:
					select_bak = select;
					select = 0;
					if (select_bak != select)
					{
						DisplayItem(0,select_bak,select);
					}
					break;
				case KEYENTER:
					EnterItem(select,select);				
					ScrCls();
					DisplayItem(1,select_bak,select);
					ScrSetFgColor(RGB(194,196,200));
					break;
					/*
				case KEY1:
				case KEY2:
				case KEY3:
				case KEY4:
					select_bak = select;
					select = ch - KEY1;
					EnterItem(select_bak,select);
					ScrCls();
					DisplayItem(1,select_bak,select);
					ScrSetFgColor(RGB(194,196,200));
					break;
					*/
				default:
					break;
			}
		}

		if(WFflag)
		{
			if(WifiCheck(NULL) < 0)
			{
				WifiRelogin();
			}
		}
	}
#endif	
}

void socket_flush(void)
{
	int iRet;
	uchar buf[1024];
	if (Socket > 0)
	{
		while(1)
		{
			iRet = NetRecv(Socket, buf, sizeof(buf), 0);
			if (iRet == 0)
			{
				return;
			}
		}
	}
}
void key_wait(long ms)
{
	TimerSet(1, ms / 100);
	while(1)
	{
		if (!kbhit())
		{
			getkey();
			return;
		}
		if (TimerCheck(1)==0)
		{
			return;
		}
	}
}

// Trade_Demo For exhibition
void Credit_Trade(void)
{
	uchar aucOut[200];
	uchar aucOther[200];
    uchar ucCardType = 0;
    int   iRet = 0;
	uchar ucRet = 0;
    uchar ucKey = 0;
	uchar logo[8092];
	char buf[128];
	uchar string[2048];
	int  i = 0;
	int Error =0;
	uchar ErrorCode =0;
	uchar szTrack1[256],szTrack2[256],szTrack3[256];
	uchar Atr[32];
	uchar uPiccFlag = 0;
	
	memset(Atr,0,sizeof(Atr));
    memset(logo,0x00,8092);
	memset(buf,0x00,128);
 	memset(string,0x00,1024);
	memset(amount,0x00,20);

	/*
	iRet = WifiCheck(NULL);
	if (iRet>0)
	{
		wifiStatus = 1;
	}
	else
	{
		wifiStatus = 0;
	}
	*/
	wifiStatus = 0 ;


	while(1)
	{   
		memset(s_RecvBuf,0x00,sizeof(s_RecvBuf));
		if(!kbhit()&&getkey()==KEYCANCEL)
    	{

		   return ;
    	}
		kbflush();	
		Dispsuspend();//  waiting  interface
		if (gucTerminalInfo[11]==1 && wifiStatus==1)
		{
			if (Socket > 0)
			{
				if (Netioctl(Socket, CMD_EVENT_GET, SOCK_EVENT_ERROR))
				{
					Socket = -1;
				}
			}
			if (Socket < 0)
			{
				iRet = WifiAcceptClient();
				if (iRet == -1)//KEYCANCEL
				{
					return;
				}
			}
			else
			{
				socket_flush();//清除socket缓存
			}
		}
		if (gucTerminalInfo[20] == 1)
		{
			PortReset(COM_BT);
		}

        //iRet = BtStatusCheck();
	    //if(iRet != 0)
	    // {
	    //   return ;
	    //}
		
		iRet = MagMessage(amount);//接收cash 数据	
		ComSends("[%s,%d]iRet=%d\r\n",FILELINE,iRet);
		if (iRet == -1)//KEYCANCEL
		{
			return;
		}
		if (iRet != OK)
		{
			DisplayBang();
			getkey();
			return;
		}

				
		DisplayAmount();	
		ScrTextOut(140,90,amount);	

		while(1)
		{ 
			ucKey = getkey();
			if(ucKey ==KEYCANCEL)///????
			{
				CancelPackage();
				return ;
			}
			else if(ucKey ==KEYENTER)
			{
				break;
			}
		}
		DisplayMagswipe();//swipe the magic card     
		MagOpen();
		PiccOpen();
		IccInit(0,Atr);
		TimerSet(0,30 * 10);
		while(1)
		{   
    	
			ucRet= MagSwiped();
			ScrGotoxyEx(0,0);
			//Lcdprintf("  MagSwiped %02x",ucRet);
			//Lcdprintf("\n");
	        uPiccFlag = 0;
			if (!ucRet)
			{
          		
				ucRet = MagRead(szTrack1, szTrack2, szTrack3);		
				ComSends("MagRead %02x",ucRet);	
				//getkey();
	
				if(ucRet)
				{
					Beep();
					DelayMs(500); 
					PortSends(0,(char *)szTrack1,strlen(szTrack1));
					PortSends(0,(char *)szTrack2,strlen(szTrack2));
					PortSends(0,(char *)szTrack3,strlen(szTrack3));
				}
				break;
			}
			
			if (!PiccDetect(0,&ucCardType,aucOut,NULL,aucOther)
				||!PiccDetect('M',&ucCardType,aucOut,NULL,aucOther))
			{
				Beep();
				DelayMs(500);        
				uPiccFlag=1;
				break;
			}
			
			if(!IccDetect(0))
			{
				Beep();
				DelayMs(500); 
				break;
			}

			if (!kbhit())
			{
				ucKey = getkey();
				if (ucKey == KEYCANCEL)  //cancel???
				{
					CancelPackage();
					return ;
				}
			}
     
			if (!TimerCheck(0))
			{   
				CancelPackage();
				return;
			}
		
		}


	    PiccClose();
		MagClose();    
		IccClose(0);
	
		StringTransparent(1);   

		if(uPiccFlag==0)
		{
	    
			DispPassWordInput();        
	 
			ScrGotoxyEx(135,125);
		
			ErrorCode = GetString(buf,0xfd,0,6);//length is 6;input the password	
			if(ErrorCode==0x0d)
			{
                ErrorCode = 0x00;
			}
			ScrSetBgColor(RGB(0,0,0));
			ScrSetFgColor(RGB(255,255,255));
			StringTransparent(0);   
			if(ErrorCode)
			{
				ComSends("err %d",ErrorCode);
				CancelPackage();
				return ;
			}


		}
		
		DisplayTrading();
			
		//ucRet= SendPrintString("Type:SALE\nAmout:$0.99\nDate:2012-03-15\n",ErrorCode);	
 	
		ComSends("\n Receipt");
		Receipt(string);

		
	   // iRet = BtStatusCheck();
	   // if(iRet != 0)
	   // {
	   //   return ;
	   // }
		
		iRet = SendPrintString(string,ErrorCode);	
		ComSends("\n ucRet %d",ucRet);
		
		if(iRet!=0)
		{
			DispTradingFail();
			getkey();
			return;
		}
				
		if (gucTerminalInfo[1] == 'T')//有打印机时打印
		{
			DispReceiptPrinting(); //print the receipt 
			
			//iRet = BtStatusCheck();
		    //if(iRet != 0)
		    //{
		     // return ;
		    //}
			PrintReceipt(logo); 
			Error = Print_Receipt(logo);	
		}
		else
		{
			Error = NO_PRINTER;
		}


		//iRet = BtStatusCheck();
		//if(iRet != 0)
		//{
		//   return ;
		//}

		
		iRet = TradeResponse(Error);

		if (iRet==0)
		{
			DispOverTrading();
		   
		}
		else
		{
			if (Error == NO_PRINTER)
			{
				DispOverTrading();
			}
			else
			{
				DispTradingFail();
			}
		}
		key_wait(6000);
	} 

}

int BtStatusCheck()
{   
        int iRet = 0;
		uchar gucBtStatus[7];

        iRet = BtGetStatus(gucBtStatus);//if the BT connection was break off,it will return. 
		return iRet ;
		
}
int CheckLanguage(void)
{
	if (strstr(LANGUAGE, "English") != NULL)
	{
		return 1;
	}
	else if (strstr(LANGUAGE, "Russian") != NULL)
	{
		return 2;
	}
	return 1;
}

void DisplayCommandWait()
{
	char str1[256];
	int strlen1;
	int btMode = 100 ;
	void* test1 ;
	
	memset(str1, 0, sizeof(str1));
	
	//BtIoCtrl( eBtCmdGetLinkStatus , test1 , 0 , test1 , 0 ) ;

	ScrCls();
	Lcdprintf( "btMode:%d\n" , btMode ) ;
	getkey();

	strcpy(str1, "Waiting for Command");
	strlen1 = strlen(str1);

	ScrCls();
	ScrProcessImage("colixo.png",tiftPng,tictDisplay,
			70,20); 
	//ScrProcessImage("swipe.png",tiftPng,tictDisplay,30,76);
	ScrTextOut(50+((320-100-strlen1*12)/2),110,str1);
}

void DisplayInsertCard()
{
	char str1[256];
	int strlen1;
	memset(str1, 0, sizeof(str1));

	strcpy(str1, "Please Insert Card");
	strlen1 = strlen(str1);

	ScrCls();
	//ScrProcessImage("swipe.png",tiftPng,tictDisplay,30,76);
	ScrProcessImage("tradinginactive.png",tiftPng,tictDisplay,
			70,20); 
	ScrTextOut(50+((320-100-strlen1*12)/2),110,str1);
}

void DisplayProcessing()
{
	char str1[256];
	int strlen1;
	memset(str1, 0, sizeof(str1));

	strcpy(str1, "Processing...");
	strlen1 = strlen(str1);

	ScrCls();
	//ScrProcessImage("swipe.png",tiftPng,tictDisplay,30,76);
	//ScrTextOut(100+((320-100-strlen1*12)/2),90,str1);
	ScrProcessImage("tradingactive.png",tiftPng,tictDisplay,
			70,20); 
	ScrTextOut(50+((320-100-strlen1*12)/2),110,str1);

}

// Trade_Demo For exhibition
void WaitingStateEngine(void)
{
	uchar aucOut[200];
	uchar aucOther[200];
    uchar ucCardType = 0;
    int   iRet = 0;
	uchar ucRet = 0;
    uchar ucKey = 0;
	uchar logo[8092];
	char buf[128];
	uchar string[2048];
	int  i = 0;
	int Error =0;
	uchar ErrorCode =0;
	uchar szTrack1[256],szTrack2[256],szTrack3[256];
	uchar Atr[32];
	uchar uPiccFlag = 0;
	
	memset(Atr,0,sizeof(Atr));
    memset(logo,0x00,8092);
	memset(buf,0x00,128);
 	memset(string,0x00,1024);
	memset(amount,0x00,20);

	/*
	iRet = WifiCheck(NULL);
	if (iRet>0)
	{
		wifiStatus = 1;
	}
	else
	{
		wifiStatus = 0;
	}
	*/
	wifiStatus = 0 ;


	while(1)
	{   
		memset(s_RecvBuf,0x00,sizeof(s_RecvBuf));
		if(!kbhit()&&getkey()==KEYCANCEL)
    	{
		   return ;
    	}
		kbflush();	
		DisplayCommandWait() ;
		if (gucTerminalInfo[11]==1 && wifiStatus==1)
		{
			if (Socket > 0)
			{
				if (Netioctl(Socket, CMD_EVENT_GET, SOCK_EVENT_ERROR))
				{
					Socket = -1;
				}
			}
			if (Socket < 0)
			{
				iRet = WifiAcceptClient();
				if (iRet == -1)//KEYCANCEL
				{
					return;
				}
			}
			else
			{
				socket_flush();//清除socket缓存
			}
		}
		if (gucTerminalInfo[20] == 1)
		{
			PortReset(COM_BT);
		}

        //iRet = BtStatusCheck();
	    //if(iRet != 0)
	    // {
	    //   return ;
	    //}
		
		//iRet = MagMessage(amount);//接收cash 数据	
		iRet = WaitingStateProcess() ;
		if (iRet == KEYCANCEL)//KEYCANCEL
		{
			return;
		}
	}
}

///////////////////////////////////////
/// @brief: Recieve message of amount
/// @return:amount 
/// @note:
/// @see:
///////////////////////////////////////

int  WaitingStateProcess(void)//recieve the transaction amount,amount as output
{
	uchar APIRet = 0 ;
	ST_UPOS_MSG  pMsg={0};
	ushort ErrorCode = ERR_OK;
	ushort PackageSize ; 
	uchar StartChar = 0x08;
	long lError;
	uchar PackageType;
	int Param_len;
	int i;
	int paramLenRemaining = 0 ;
	unsigned char bufferMsg[32] ;

	unsigned char apduRequest[512] ;
	unsigned char apduResponse[512] ;
	int apduRequestLen ;
	int apduResponseLen ;

	int line = 0 ;

	//TimerSet(0,1000);//set timers

	while(1)
	{
		i= RecvStartChar(); 
		if (i == -1) return -1;
		if (i == -2) return -2;
		lError = ReceiveMsg(&PackageType);	
		ComSends("[%s,%d]lError=%d,Type=%d\r\n",FILELINE,lError,PackageType);
		if(PackageType!=1)  //correct Package
		{
			return 1;      //error package
			// continue;
		}	
		if(ERR_OK != lError)
		{
			APIRet = PortReset(gucComPort);
			continue;
		}
		else
		{
			break;
		}
	}

	ScrCls() ;
    Lcdprintf( "Header %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x" , 
				s_RecvBuf[0],
				s_RecvBuf[1],
				s_RecvBuf[2],
				s_RecvBuf[3],
				s_RecvBuf[4],
				s_RecvBuf[5],
				s_RecvBuf[6],
				s_RecvBuf[7],
				s_RecvBuf[8],
				s_RecvBuf[9],
				s_RecvBuf[10],
				s_RecvBuf[11]
				) ;
	getkey() ;

	Param_len = (s_RecvBuf[3] << 8) | (s_RecvBuf[4]);
	ScrCls() ;
    Lcdprintf( "Message Len:\n%d " , Param_len ) ;
	getkey() ;

	memset(apduRequest,0x00,Param_len+1);
	memcpy(apduRequest,s_RecvBuf+5,Param_len); 
	paramLenRemaining = 0 ;	

	ScrCls() ;
	for( paramLenRemaining = 0 ; paramLenRemaining < Param_len ; paramLenRemaining++)
	{
		Lcdprintf( "%02x " , s_RecvBuf[paramLenRemaining+5] ) ;
	}
	getkey() ;

	memcpy(s_SendBuf, &StartChar, sizeof(uchar));//add the package head
	
	memset( apduResponse, 0, sizeof( apduResponse ) ) ;

	if( iccDetectCard() != 0 )
	{
		DisplayInsertCard() ;
		while( 1 )
		{
			if(!kbhit()&&getkey()==KEYCANCEL)
			{
				return KEYCANCEL ;
			}
			else if( iccDetectCard() == 0 )
			{
				break ;
			}
		}
	}

	iccStartCmd() ;
	iccSendCmd( apduRequest , 0 , apduResponse , &apduResponseLen ) ;
	iccStopCmd() ;

	pMsg.PackageType = 0x01;  		                                                                               
    //PackageSize= 0x05;
	PackageSize = apduResponseLen+2 ;
	
    PutDatatoBuffer(pMsg.PackageSize, PackageSize, E_DATA_TYPE_SHORT);
    memcpy(s_SendBuf + 1, &pMsg, sizeof(pMsg));
	
	s_SendBuf[4] = lError;
	memcpy( s_SendBuf+5 , apduResponse , apduResponseLen ) ;

	s_lSendBufLen = 6 + apduResponseLen ;
	
	//kdelacruz - hardcoded
	//return -1 ;
    return SendResponse(s_SendBuf, s_lSendBufLen);
}
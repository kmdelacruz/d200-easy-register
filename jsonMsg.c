#include <jsonMsg.h>   
#include <jsmn.h>
#include <posapi.h>
#include <posapi_all.h>
#include "global.h"

int jsonInitializerBuildPayment( void )
{
	hostBuildListSizePymt = 0 ;

	hostBuildListPymt[hostBuildListSizePymt++].elementID = PYMTBRKR_FIELD_LONGITUDE ;
	hostBuildListPymt[hostBuildListSizePymt++].elementID = PYMTBRKR_FIELD_LATITUDE ;
	hostBuildListPymt[hostBuildListSizePymt++].elementID = TOKEN_PRIMID_PAYLOAD ;
	hostBuildListPymt[hostBuildListSizePymt++].elementID = PYMTBRKR_FIELD_TXNAMOUNT ;
	hostBuildListPymt[hostBuildListSizePymt++].elementID = PYMTBRKR_FIELD_TIP ;
}

int jsonInitializerBuildRegistration( void )
{
	hostDataListSizeRgst = 0 ;

	hostDataListRgst[hostDataListSizeRgst++].elementID = REGISTER_FIELD_USERNAME ;
	hostDataListRgst[hostDataListSizeRgst++].elementID = REGISTER_FIELD_PASSWORD ;
	hostDataListRgst[hostDataListSizeRgst++].elementID = REGISTER_FIELD_VTID ;
}

int jsonInitializerParserPrimitives( void )
{
	hostDataListSizePrim = 0 ;

	hostDataListPrim[hostDataListSizePrim].elementID = TOKEN_PRIMID_COMMAND ; 
	hostDataListPrim[hostDataListSizePrim].valueSize = DATACONTAINER_SIZE_128 ;
	hostDataListPrim[hostDataListSizePrim++].value = g_dataContainer.cmdID ;
	
	/* // payload is special
	hostDataListPymt[hostDataListSizePymt].elementID = TOKEN_PRIMID_PAYLOAD ; 
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_128 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.payload ;
	*/

	hostDataListPrim[hostDataListSizePrim].elementID = TOKEN_PRIMID_POSTBACKURI ; 
	hostDataListPrim[hostDataListSizePrim].valueSize = DATACONTAINER_SIZE_256 ;
	hostDataListPrim[hostDataListSizePrim++].value = g_dataContainer.postBackUri ;

	hostDataListPrim[hostDataListSizePrim].elementID = TOKEN_PRIMID_MESSAGE ; 
	hostDataListPrim[hostDataListSizePrim].valueSize = DATACONTAINER_SIZE_256 ;
	hostDataListPrim[hostDataListSizePrim++].value = g_dataContainer.message ;

	hostDataListPrim[hostDataListSizePrim].elementID = TOKEN_PRIMID_KSN ; 
	hostDataListPrim[hostDataListSizePrim].valueSize = DATACONTAINER_SIZE_256 ;
	hostDataListPrim[hostDataListSizePrim++].value = g_dataContainer.ksn ;

	hostDataListPrim[hostDataListSizePrim].elementID = TOKEN_PRIMID_SESSIONID ; 
	hostDataListPrim[hostDataListSizePrim].valueSize = DATACONTAINER_SIZE_256 ;
	hostDataListPrim[hostDataListSizePrim++].value = g_dataContainer.sessionId ;

}

int jsonInitializerParserPayment( void )
{
	hostDataListSizePymt = 0 ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_AID ; 
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.AID ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_AIDSCHEME ; 
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.aidScheme ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_AMOUNT ; 
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.transactionAmount ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_APPROVALCODE ; 
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.approvalCode ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_BATCHID ; 
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.batchID ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_BRANCHID ; 
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_64 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.branchID ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_CARDHOLDERNAME ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_64 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.cardholderName ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_CARDSEQNUM ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.cardSequenceNumber ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_CURRENCYCODE ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.currencyCode ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_EMVCHIPDATA ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_256 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.emvChipData ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_ENTRYMODE ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.entryMode ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_ID ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_64 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.ID ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_INVOICENUM ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.invoiceNum ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_LATITUDE ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.latitude ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_LONGITUDE ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.longitude ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_MASKEDPAN ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_16 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.maskedPAN ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_MID ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_16 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.mid ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_PANTOKEN ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_64 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.panToken ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_POSCONDCODE ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.posCondition ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_REFNO ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_16 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.rrn ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_RESPONSECODE ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.respCode ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_RQSTTIMESTAMP ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.requestTimestamp ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_STAN ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.stan ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_STATUS ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_64 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.status ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_TC ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_64 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.tc ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_TID ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_16 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.termID ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_TIMESTAMP ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_64 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.timeStamp ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_TIP ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_16 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.tip ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_TRANSACTIONTYPE ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_8 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.tranType ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_TRACK2DATA ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.track2Data ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_TXNAMOUNT  ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.transactionAmount ;

	hostDataListPymt[hostDataListSizePymt].elementID = PYMTBRKR_FIELD_TXNAMOUNT  ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.transactionAmount ;

	hostDataListPymt[hostDataListSizePymt].elementID = REGISTER_FIELD_USERNAME  ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.userName ;

	hostDataListPymt[hostDataListSizePymt].elementID = REGISTER_FIELD_PASSWORD  ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_32 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.passWord ;

	hostDataListPymt[hostDataListSizePymt].elementID = REGISTER_FIELD_VTID  ;
	hostDataListPymt[hostDataListSizePymt].valueSize = DATACONTAINER_SIZE_16 ;
	hostDataListPymt[hostDataListSizePymt++].value = g_dataContainer.vtID ;

}

//**********************
//parser functions
//**********************
int jsonParseDataGeneric( char * dataBuffer ,  jsmntok_t * tokenPtr , int tokenPtrElmnt , char * jsBuffer , int dataBufferSize)
{
	ScrCls() ;
	ScrPrint( 0,0,0, "jsonParseDataGeneric" ) ;
	ScrPrint( 0,1,0, "tokenPtrElmnt:%d" , tokenPtrElmnt ) ;
	ScrPrint( 0,2,0, "tokenStart:%d" , tokenPtr[tokenPtrElmnt].start ) ;
	ScrPrint( 0,3,0, "tokenEnd:%d" , tokenPtr[tokenPtrElmnt].end ) ;
	ScrPrint( 0,4,0, "dataBufSize:%d" , dataBufferSize ) ;
	getkey();
	memset( dataBuffer , 0 , dataBufferSize ) ;
	strncpy( dataBuffer , (char*)jsBuffer+tokenPtr[tokenPtrElmnt+1].start , tokenPtr[tokenPtrElmnt+1].end - tokenPtr[tokenPtrElmnt+1].start ) ;
	ScrCls() ;
	ScrPrint( 0,0,0, "jsonParseDataGeneric" ) ;
	ScrPrint( 0,2,0, "%s" , dataBuffer ) ;
	getkey();
	return tokenPtrElmnt + 1 ;

}

int	jsonPymntBrkrParser( g_dataContainerBuffer * dataBuffer , const char * jsBuffer )
{
	int r;
	jsmn_parser p;
	jsmntok_t tokens[100];
	char * buffer[COMM_BUFFER_MAX_LEN] ;
	int ctr ;
	int listCtr = 0 ;

	memset( &tokens , 0 , sizeof( jsmntok_t ) ) ; 

	strcpy ( jsBuffer ,  "commandType\": \"TRANSMIT\", \
		\"payload\": { \
        \"CB (Credit)\": \"E07781E927A67BC0FE86ACC82DF1C91A515557417D747829\", \
        \"MasterCard Credit\": \"E07781E927A67BC0F9D6A2117407AFF842DAF9040E98AAD3\", \
        \"Maestro UK (formerly branded as Switch)\": \"E07781E927A67BC04050B64A9F797B33B340E94D6F124454\", \
        \"Credit\": \"F755CB3CFD17D4EFCDC0CE7AB49636BECAE62F0C829D8896\", \
        \"Electronic Cash\": \"F755CB3CFD17D4EF9B53D2ED47948F5041A612BD73618417\", \
        \"PagoBANCOMAT\": \"11167732F98C4E64D9DD70E9901FCEBCF238DA8AFACA4F7E\", \
        \"V PAY\": \"E07781E927A67BC0DFD1CEC4380477010ECE4BC0D4ACE3D3\", \
        \"RuPay\": \"E07781E927A67BC01CD7BEACCD0886422B6324786BAF7E76\", \
        \"Maestro (debit card)\": \"E07781E927A67BC0A0F8BBE1E2ADBA19ADB6D6BAD4FDBBDB\", \
        \"Banricompras Debito\": \"E07781E927A67BC0B1A820FA6BE80AAF4C0DD701D67B9484\", \
        \"ZIP\": \"E07781E927A67BC0CCB7BC756A53F46EC0D3E8F0FF812F7F\", \
        \"Cirrus (interbank network) ATM card only\": \"E07781E927A67BC02B1A0D89224775261A6BC7F218781AD1\", \
        \"Quasi Credit\": \"F755CB3CFD17D4EF510B4E905176DC7A3B28F27B6ADC24D9\", \
        \"Debit card\": \"E07781E927A67BC09C78DB484E507759888D2EDF1B20A207\", \
        \"Visa Credit\": \"E07781E927A67BC022A0B90BC981ECA3C034C2F7985E0C3F\", \
        \"Japan Credit Bureau\": \"E07781E927A67BC0550BDB65D555F7B48CF7C9E93E606311\", \
        \"Universal Electronic Card\": \"E07781E927A67BC06718362FB9D41C104836A82345E317BA\", \
        \"American Express\": \"03E902F4888B6BB71C8E9B11F9928ADC\", \
        \"Plus\": \"E07781E927A67BC0593782D29B419EEB1641FEF340944A50\", \
        \"MasterCard[4]\": \"E07781E927A67BC016BD84C52103C19B3F91C15466C25C50\", \
        \"Girocard\": \"CF3716B355D6BD18B06458CD2B28D42A5CEC5C304799CDFB\", \
        \"SPAN\": \"11167732F98C4E646C47DFD495A510617CCEF87A8045CB59\", \
        \"Verve\": \"E07781E927A67BC091CC1DEB1E691B10AF42422A0BAC74AB\", \
        \"Diners Club/Discover\": \"E07781E927A67BC0444AE7B24FA8ACEBCA16D14DF1DC42F1\", \
        \"ATM card\": \"E07781E927A67BC0E26A4C53B335E8462BCFE2539B505690\", \
        \"CB (Debit card only)\": \"E07781E927A67BC08E785F5AC2E592748C1E21CE71CD3D37\", \
        \"Debit\": \"F755CB3CFD17D4EF3151D7A7A23C1A76559D4836CDA8096F\", \
        \"Visa Electron\": \"E07781E927A67BC08722B99593129BAE80EADBFD0E1D9334\" \
		},\
		\"postbackUri\": \"/candidate-list\",\
		\"message\": null ;"  
	) ;

	ScrCls() ;
	ScrPrint( 0,0,0, "jsonPymntBrokerParser" ) ;
	ScrPrint( 0,1,0, "jsmn_parse" ) ;
	getkey() ;

	jsmn_init(&p);
	r = jsmn_parse(&p, jsBuffer, strlen(jsBuffer), tokens, 100);

	ScrCls() ;
	ScrPrint( 0,0,0, "jsonPymntBrokerParser" ) ;
	ScrPrint( 0,1,0, "r:%d" , r ) ;
	getkey() ;

	for( ctr = 0 ; ctr < r ; ctr++ )
	{
		memset( buffer , 0 , sizeof( buffer ) ) ;
		ScrCls() ;
		ScrPrint( 0,0,0, "token[%d]" , ctr ) ;
		ScrPrint( 0,1,0, "type:%d" , tokens[ctr].type ) ;
		ScrPrint( 0,2,0, "start:%d" , tokens[ctr].start ) ;
		ScrPrint( 0,3,0, "end:%d" , tokens[ctr].end ) ;
		ScrPrint( 0,4,0, "size:%d" , tokens[ctr].size ) ;
		strncpy( ( char*)buffer , jsBuffer+tokens[ctr].start , tokens[ctr].end - tokens[ctr].start ) ; 
		ScrPrint( 0,5,0, "%s" , buffer ) ;
		getkey();

		//need to improve logic 
		//original design is using function ptr
		//for refactoring purposes
		for( listCtr = 0 ; listCtr < hostDataListSizePymt ; listCtr++ )
		{
			if( strcmp( (char*)buffer , hostDataListPymt[listCtr].elementID ) == 0 )
			{
				ctr = jsonParseDataGeneric( hostDataListPymt[listCtr].value , tokens , ctr , (char*)jsBuffer , hostDataListPymt[listCtr].valueSize );
				//force listCtr out
				listCtr = hostDataListSizePymt ;

			}
		}

		ScrCls() ;
		ScrPrint( 0,0,0, "token[%d]" , ctr ) ;
		ScrPrint( 0,1,0, "type:%d" , tokens[ctr].type ) ;
		ScrPrint( 0,2,0, "start:%d" , tokens[ctr].start ) ;
		ScrPrint( 0,3,0, "end:%d" , tokens[ctr].end ) ;
		ScrPrint( 0,4,0, "size:%d" , tokens[ctr].size ) ;
		ScrPrint( 0,5,0, "ctr:%d" , ctr ) ;
		strncpy( ( char*)buffer , jsBuffer+tokens[ctr].start , tokens[ctr].end - tokens[ctr].start ) ; 
		ScrPrint( 0,6,0, "%s" , buffer ) ;
		getkey();

	}

	return 0;	

}

int jsonEmvParser( g_dataContainerBuffer * dataBuffer , const char * jsBuffer )
{
	int r;
	jsmn_parser p;
	jsmntok_t tokens[100];
	char * buffer[COMM_BUFFER_MAX_LEN] ;
	int ctr ;
	int listCtr = 0 ;

	memset( &tokens , 0 , sizeof( jsmntok_t ) ) ; 

	strcpy ( jsBuffer ,  "commandType\": \"TRANSMIT\", \
		\"payload\": { \
        \"CB (Credit)\": \"E07781E927A67BC0FE86ACC82DF1C91A515557417D747829\", \
        \"MasterCard Credit\": \"E07781E927A67BC0F9D6A2117407AFF842DAF9040E98AAD3\", \
        \"Maestro UK (formerly branded as Switch)\": \"E07781E927A67BC04050B64A9F797B33B340E94D6F124454\", \
        \"Credit\": \"F755CB3CFD17D4EFCDC0CE7AB49636BECAE62F0C829D8896\", \
        \"Electronic Cash\": \"F755CB3CFD17D4EF9B53D2ED47948F5041A612BD73618417\", \
        \"PagoBANCOMAT\": \"11167732F98C4E64D9DD70E9901FCEBCF238DA8AFACA4F7E\", \
        \"V PAY\": \"E07781E927A67BC0DFD1CEC4380477010ECE4BC0D4ACE3D3\", \
        \"RuPay\": \"E07781E927A67BC01CD7BEACCD0886422B6324786BAF7E76\", \
        \"Maestro (debit card)\": \"E07781E927A67BC0A0F8BBE1E2ADBA19ADB6D6BAD4FDBBDB\", \
        \"Banricompras Debito\": \"E07781E927A67BC0B1A820FA6BE80AAF4C0DD701D67B9484\", \
        \"ZIP\": \"E07781E927A67BC0CCB7BC756A53F46EC0D3E8F0FF812F7F\", \
        \"Cirrus (interbank network) ATM card only\": \"E07781E927A67BC02B1A0D89224775261A6BC7F218781AD1\", \
        \"Quasi Credit\": \"F755CB3CFD17D4EF510B4E905176DC7A3B28F27B6ADC24D9\", \
        \"Debit card\": \"E07781E927A67BC09C78DB484E507759888D2EDF1B20A207\", \
        \"Visa Credit\": \"E07781E927A67BC022A0B90BC981ECA3C034C2F7985E0C3F\", \
        \"Japan Credit Bureau\": \"E07781E927A67BC0550BDB65D555F7B48CF7C9E93E606311\", \
        \"Universal Electronic Card\": \"E07781E927A67BC06718362FB9D41C104836A82345E317BA\", \
        \"American Express\": \"03E902F4888B6BB71C8E9B11F9928ADC\", \
        \"Plus\": \"E07781E927A67BC0593782D29B419EEB1641FEF340944A50\", \
        \"MasterCard[4]\": \"E07781E927A67BC016BD84C52103C19B3F91C15466C25C50\", \
        \"Girocard\": \"CF3716B355D6BD18B06458CD2B28D42A5CEC5C304799CDFB\", \
        \"SPAN\": \"11167732F98C4E646C47DFD495A510617CCEF87A8045CB59\", \
        \"Verve\": \"E07781E927A67BC091CC1DEB1E691B10AF42422A0BAC74AB\", \
        \"Diners Club/Discover\": \"E07781E927A67BC0444AE7B24FA8ACEBCA16D14DF1DC42F1\", \
        \"ATM card\": \"E07781E927A67BC0E26A4C53B335E8462BCFE2539B505690\", \
        \"CB (Debit card only)\": \"E07781E927A67BC08E785F5AC2E592748C1E21CE71CD3D37\", \
        \"Debit\": \"F755CB3CFD17D4EF3151D7A7A23C1A76559D4836CDA8096F\", \
        \"Visa Electron\": \"E07781E927A67BC08722B99593129BAE80EADBFD0E1D9334\" \
		},\
		\"postbackUri\": \"/candidate-list\",\
		\"message\": null ;"  
	) ;

	ScrCls() ;
	ScrPrint( 0,0,0, "jsonEmvParser" ) ;
	ScrPrint( 0,1,0, "jsmn_parse" ) ;
	getkey() ;

	jsmn_init(&p);
	r = jsmn_parse(&p, jsBuffer, strlen(jsBuffer), tokens, 100);

	ScrCls() ;
	ScrPrint( 0,0,0, "jsonEmvParser" ) ;
	ScrPrint( 0,1,0, "r:%d" , r ) ;
	getkey() ;

	for( ctr = 0 ; ctr < r ; ctr++ )
	{
		memset( buffer , 0 , sizeof( buffer ) ) ;
		ScrCls() ;
		ScrPrint( 0,0,0, "token[%d]" , ctr ) ;
		ScrPrint( 0,1,0, "type:%d" , tokens[ctr].type ) ;
		ScrPrint( 0,2,0, "start:%d" , tokens[ctr].start ) ;
		ScrPrint( 0,3,0, "end:%d" , tokens[ctr].end ) ;
		ScrPrint( 0,4,0, "size:%d" , tokens[ctr].size ) ;
		strncpy( ( char*)buffer , jsBuffer+tokens[ctr].start , tokens[ctr].end - tokens[ctr].start ) ; 
		ScrPrint( 0,5,0, "%s" , buffer ) ;
		getkey();

		for( listCtr = 0 ; listCtr < hostDataListSizePrim ; listCtr++ )
		{

				ScrCls() ;
				ScrPrint( 0,0,0, "hostDataList[%d]" , listCtr ) ;
				ScrPrint( 0,1,0, "hostDataListSizePymt:%d" , hostDataListSizePymt ) ;
				ScrPrint( 0,2,0, "elementId" ) ;
				ScrPrint( 0,3,0, "%s" , hostDataListPrim[listCtr].elementID ) ;
				getkey();

			if( strcmp( (char*)buffer , TOKEN_PRIMID_PAYLOAD ) == 0 )
			{
				ctr = jsonParseEmvSectionPayload( dataBuffer , tokens , ctr , (char*)jsBuffer ) ;
				//force listCtr out
				listCtr = hostDataListSizePrim ;
			}
			else if( strcmp( (char*)buffer , hostDataListPrim[listCtr].elementID ) == 0 )
			{
				ctr = jsonParseDataGeneric( hostDataListPrim[listCtr].value , tokens , ctr , (char*)jsBuffer , hostDataListPrim[listCtr].valueSize );
				//force listCtr out
				listCtr = hostDataListSizePrim ;

			}
		}

		ScrCls() ;
		ScrPrint( 0,0,0, "token[%d]" , ctr ) ;
		ScrPrint( 0,1,0, "type:%d" , tokens[ctr].type ) ;
		ScrPrint( 0,2,0, "start:%d" , tokens[ctr].start ) ;
		ScrPrint( 0,3,0, "end:%d" , tokens[ctr].end ) ;
		ScrPrint( 0,4,0, "size:%d" , tokens[ctr].size ) ;
		ScrPrint( 0,5,0, "ctr:%d" , ctr ) ;
		strncpy( ( char*)buffer , jsBuffer+tokens[ctr].start , tokens[ctr].end - tokens[ctr].start ) ; 
		ScrPrint( 0,6,0, "%s" , buffer ) ;
		getkey();

	}
	return 0;	
}

int jsonParseEmvSectionCommand( g_dataContainerBuffer * jsonEmvRcvBuf , jsmntok_t * tokenPtr , int tokenPtrElmnt , char * jsBuffer ) 
{
	memset( jsonEmvRcvBuf->cmdID , 0 , DATACONTAINER_SIZE_128 ) ;
	strncpy( jsonEmvRcvBuf->cmdID , (char*)jsBuffer[tokenPtr[tokenPtrElmnt+1].start] , tokenPtr[tokenPtrElmnt+1].end - tokenPtr[tokenPtrElmnt+1].start ) ;
	ScrCls() ;
	ScrPrint( 0,0,0, "jsonParseCmd" ) ;
	ScrPrint( 0,1,0, "tokenPtrElmnt:%d" , tokenPtrElmnt ) ;
	ScrPrint( 0,2,0, "tokenStart:%d" , tokenPtr[tokenPtrElmnt].start ) ;
	ScrPrint( 0,3,0, "tokenEnd:%d" , tokenPtr[tokenPtrElmnt].end ) ;
	ScrPrint( 0,4,0, "%s" , jsonEmvRcvBuf->cmdID ) ;
	getkey();
	return tokenPtrElmnt + 1 ;
}

int jsonParseEmvSectionPayload( g_dataContainerBuffer * jsonEmvRcvBuf , jsmntok_t * tokenPtr , int tokenPtrElmnt , char * jsBuffer ) 
{
	int curLen = 0 ;
	int payLoadCtr = tokenPtrElmnt+2 ;
	int bufferIdx = 0 ;
	/*
	ScrCls();
	ScrPrint( 0,0,0, "jsonParsePayload" ) ;
	ScrPrint( 0,1,0, "tokenPtrElmnt:%d" , tokenPtrElmnt ) ;
	ScrPrint( 0,2,0, "tokenPtrSize[%d]:%d" , tokenPtrElmnt+1 , tokenPtr[tokenPtrElmnt+1].end - tokenPtr[tokenPtrElmnt+1].start ) ;
	ScrPrint( 0,3,0, "tokenPtrSize[%d]:%d" , tokenPtrElmnt+2 , tokenPtr[tokenPtrElmnt+2].end - tokenPtr[tokenPtrElmnt+2].start ) ;
	ScrPrint( 0,4,0, "tokenPtrSize[%d]:%d" , tokenPtrElmnt+3 , tokenPtr[tokenPtrElmnt+3].end - tokenPtr[tokenPtrElmnt+3].start ) ;
	ScrPrint( 0,6,0, "tokenPtrSize[%d]:%d" , tokenPtrElmnt , tokenPtr[tokenPtrElmnt].end - tokenPtr[tokenPtrElmnt].start ) ;
	getkey();
	*/
	jsonEmvRcvBuf->payloadSize = 0 ;

	while( curLen <= ( tokenPtr[tokenPtrElmnt+1].end - tokenPtr[tokenPtrElmnt+1].start ) ) 
	{
		memset( jsonEmvRcvBuf->payloadName[bufferIdx] , 0 , DATACONTAINER_SIZE_256 ) ;
		memset( jsonEmvRcvBuf->payloadData[bufferIdx] , 0 , DATACONTAINER_SIZE_256 ) ;
		/*
		ScrCls();
		ScrPrint( 0,0,0, "jsonParsePayload" ) ;
		ScrPrint( 0,1,0, "payLoadCtr:%d" , payLoadCtr ) ;
		ScrPrint( 0,2,0, "tokenStart:%d" , tokenPtr[payLoadCtr].start ) ;
		ScrPrint( 0,3,0, "tokenEnd:%d" , tokenPtr[payLoadCtr].end ) ;
		ScrPrint( 0,4,0, "tokenStart+1:%d" , tokenPtr[payLoadCtr+1].start ) ;
		ScrPrint( 0,5,0, "tokenEnd+1:%d" , tokenPtr[payLoadCtr+1].end ) ;
		ScrPrint( 0,6,0, "bufferIdx:%d" , bufferIdx ) ;
		getkey();
		*/
		strncpy( (char*)jsonEmvRcvBuf->payloadName[bufferIdx] , (char*)jsBuffer+tokenPtr[payLoadCtr].start , tokenPtr[payLoadCtr].end - tokenPtr[payLoadCtr].start ) ;
		/*
		ScrCls();
		ScrPrint( 0,0,0, "jsonParsePayload" ) ;
		ScrPrint( 0,1,0, "bufferIdx:%d" , bufferIdx ) ;
		ScrPrint( 0,2,0, "%s" , jsonEmvRcvBuf->payloadName[bufferIdx] ) ;
		getkey();
		*/
		strncpy( (char*)jsonEmvRcvBuf->payloadData[bufferIdx] , (char*)jsBuffer+tokenPtr[payLoadCtr+1].start , tokenPtr[payLoadCtr+1].end - tokenPtr[payLoadCtr+1].start ) ;
		/*
		ScrCls();
		ScrPrint( 0,0,0, "jsonParsePayload" ) ;
		ScrPrint( 0,1,0, "bufferIdx:%d" , bufferIdx ) ;
		ScrPrint( 0,2,0, "%s" , jsonEmvRcvBuf->payloadData[bufferIdx] ) ;
		//ScrPrint( 0,2,0, "%s" , jsBuffer+tokenPtr[payLoadCtr+1].start ) ;
		getkey();
		*/
		payLoadCtr += 2 ;
		bufferIdx++ ;
		curLen = tokenPtr[payLoadCtr+1].end ;
	}

	jsonEmvRcvBuf->payloadSize = bufferIdx ;

	ScrCls();
	ScrPrint( 0,0,0, "jsonParsePayload" ) ;
	ScrPrint( 0,1,0, "end of the function" ) ;
	ScrPrint( 0,2,0, "PayLoadCtr:%d" , payLoadCtr ) ;
	ScrPrint( 0,3,0, "payloadSize:%d" , jsonEmvRcvBuf->payloadSize ) ;
	getkey();

	return payLoadCtr + 1 ;
}

int jsonParseEmvSectionPostbackURI( g_dataContainerBuffer * jsonEmvRcvBuf , jsmntok_t * tokenPtr , int tokenPtrElmnt , char * jsBuffer ) 
{
	memset( jsonEmvRcvBuf->postBackUri , 0 , DATACONTAINER_SIZE_256 ) ;
	strncpy( jsonEmvRcvBuf->postBackUri , (char*)jsBuffer+tokenPtr[tokenPtrElmnt+1].start , tokenPtr[tokenPtrElmnt+1].end - tokenPtr[tokenPtrElmnt+1].start ) ;
	ScrCls() ;
	ScrPrint( 0,0,0, "jsonParsePostbackURI" ) ;
	ScrPrint( 0,1,0, "tokenPtrElmnt:%d" , tokenPtrElmnt ) ;
	ScrPrint( 0,2,0, "tokenStart:%d" , tokenPtr[tokenPtrElmnt+1].start ) ;
	ScrPrint( 0,3,0, "tokenEnd:%d" , tokenPtr[tokenPtrElmnt+1].end ) ;
	ScrPrint( 0,4,0, "%s" , jsonEmvRcvBuf->postBackUri ) ;
	getkey();
	return tokenPtrElmnt + 1 ;
}

int jsonParseEmvSectionMessage( g_dataContainerBuffer * jsonEmvRcvBuf , jsmntok_t * tokenPtr , int tokenPtrElmnt , char * jsBuffer ) 
{
	memset( jsonEmvRcvBuf->message , 0 , DATACONTAINER_SIZE_256 ) ;
	strncpy( jsonEmvRcvBuf->message , (char*)jsBuffer+tokenPtr[tokenPtrElmnt+1].start , tokenPtr[tokenPtrElmnt+1].end - tokenPtr[tokenPtrElmnt+1].start ) ;
	ScrCls() ;
	ScrPrint( 0,0,0, "jsonParseMessage" ) ;
	ScrPrint( 0,1,0, "tokenPtrElmnt:%d" , tokenPtrElmnt ) ;
	ScrPrint( 0,2,0, "tokenStart:%d" , tokenPtr[tokenPtrElmnt+1].start ) ;
	ScrPrint( 0,3,0, "tokenEnd:%d" , tokenPtr[tokenPtrElmnt+1].end ) ;
	ScrPrint( 0,4,0, "%s" ,  jsonEmvRcvBuf->message ) ;
	getkey();
	return tokenPtrElmnt + 1 ;
}

//**********************
//builder functions
//**********************
int jsonPayloadWrap( g_dataContainerBuffer * dataContainer , char * jsBuffer )
{
	char buffer[DATACONTAINER_SIZE_256] ;
	char payloadBuffer[DATACONTAINER_SIZE_4096] ;
	int payloadCtr = 0 ;

	memset( payloadBuffer , 0 , sizeof( payloadBuffer ) ) ;

	for( payloadCtr = 0 ; payloadCtr < 	dataContainer->payloadSize ; payloadCtr++ )
	{
		memset( buffer , 0 , sizeof( buffer ) ) ;
		sprintf( buffer , "\"%s\":\"%s\";\n" , dataContainer->payloadName[payloadCtr] , dataContainer->payloadData[payloadCtr] ) ;
		strcat( payloadBuffer , buffer ) ;
	}

	jsonPrimitiveWrap( TOKEN_PRIMID_PAYLOAD , payloadBuffer , payloadBuffer ) ;

	strcat( jsBuffer , payloadBuffer ) ;
}

int jsonPackageWrap(char * valueBuffer , char * jsBuffer)
{
	char buffer[DATACONTAINER_SIZE_8192];
	memset( buffer , 0 , sizeof( buffer ) );
	sprintf( buffer , "{%s}" , valueBuffer ) ;
	strcpy( jsBuffer , buffer ) ;
	return 0 ;
}

int jsonPrimitiveWrap( char * primitiveID , char * valueBuffer , char * jsBuffer )
{
	char buffer[DATACONTAINER_SIZE_4096];
	memset( buffer , 0 , sizeof( buffer ) ) ;
	sprintf( buffer , "%s = {\n%s\n};\n" , primitiveID , valueBuffer ) ; 
	strcpy( jsBuffer , buffer ) ;
	return 0 ;
}

int jsonAddElement( char * primitiveID , char * valueBuffer , char * jsBuffer )
{
	char buffer[256] ;

	memset( buffer , 0 , sizeof( buffer ) ) ;
	sprintf( buffer , "\"%s\":\"%s\";\n" , primitiveID, valueBuffer ) ;
	strcat( jsBuffer , buffer ) ;
	return 0 ;
}

int jsonBuildEmvSectionPayment( g_dataContainerBuffer * jsonPaymentSection , char * jsBuffer , int bufLen )
{
	int retVal ;
	int elemList = 0 ;
	int bufMapList = 0 ;

	strcpy( jsonPaymentSection->latitude , "1" ) ;
	strcpy( jsonPaymentSection->longitude , "2" ) ;
	strcpy( jsonPaymentSection->transactionAmount , "1000.00" ) ;
	strcpy( jsonPaymentSection->currencyCode , "CHF" ) ;
	strcpy( jsonPaymentSection->tip , "10.01" ) ;

	strcpy( jsonPaymentSection->payloadName[0] , "TST0" ) ;
	strcpy( jsonPaymentSection->payloadData[0] , "TestingData0" ) ;
	strcpy( jsonPaymentSection->payloadName[1] , "TST1" ) ;
	strcpy( jsonPaymentSection->payloadData[1] , "TestingData1" ) ;
	strcpy( jsonPaymentSection->payloadName[2] , "TST2" ) ;
	strcpy( jsonPaymentSection->payloadData[2] , "TestingData2" ) ;

	strcpy( jsonPaymentSection->userName , "merchant" ) ;
	strcpy( jsonPaymentSection->passWord , "merchant" ) ;
	strcpy( jsonPaymentSection->vtID , "1" ) ;


	jsonPaymentSection->payloadSize = 3 ;

	ScrCls() ;
	ScrPrint( 0,0,0, "jsonBuildEmv" ) ;
	ScrPrint( 0,1,0, "BuildSize:%d" , hostBuildListSizePymt ) ;
	ScrPrint( 0,2,0, "DataSize:%d" , hostDataListSizePymt ) ;
	getkey() ;

	memset( jsBuffer , 0 , sizeof( bufLen ) ) ;

	for( elemList = 0 ; elemList < hostBuildListSizePymt ; elemList++ )
	{
		for( bufMapList = 0 ; bufMapList < hostDataListSizePymt ; bufMapList++ )
		{
			/*
			ScrCls() ;
			ScrPrint( 0,0,0, "jsonBuildEmv" ) ;
			ScrPrint( 0,1,0, "build[%d]" , elemList ) ;
			ScrPrint( 0,2,0, "%s" , hostBuildListPymt[elemList].elementID ) ;
			ScrPrint( 0,4,0, "data[%d]" , bufMapList ) ;
			ScrPrint( 0,5,0, "%s" , hostDataListPymt[bufMapList].elementID ) ;
			ScrPrint( 0,7,0, "Cmp:%d" , strcmp( hostBuildListPymt[elemList].elementID , hostDataListPymt[bufMapList].elementID ) ) ;
			getkey() ;
			*/

			if( strcmp( hostBuildListPymt[elemList].elementID , TOKEN_PRIMID_PAYLOAD ) == 0 )
			{
				jsonPayloadWrap( &g_dataContainer , jsBuffer ) ;
				bufMapList = hostDataListSizePymt ;
			}
			else if( strcmp( hostBuildListPymt[elemList].elementID , hostDataListPymt[bufMapList].elementID ) == 0 )
			{
				jsonAddElement( hostDataListPymt[bufMapList].elementID , hostDataListPymt[bufMapList].value , jsBuffer ) ;
				bufMapList = hostDataListSizePymt ;
			}
		}
	}
	/*
	PrnInit();
	PrnStr( jsBuffer ) ;
	PrnStart() ;
	*/
	//jsonPackageWrap( jsBuffer , jsBuffer ) ;
	/*
	PrnInit();
	PrnStr( jsBuffer ) ;
	PrnStart() ;
	*/
	return retVal ;

}

int jsonBuildMessageGeneric( s_jsonBufMapList elemList[] , int maxElemList , g_dataContainerBuffer * jsonPaymentSection , char * jsBuffer , int bufLen )
{
	int retVal ;
	int elemListCtr = 0 ;
	int bufMapList = 0 ;

	strcpy( jsonPaymentSection->latitude , "1" ) ;
	strcpy( jsonPaymentSection->longitude , "2" ) ;
	strcpy( jsonPaymentSection->transactionAmount , "1000.00" ) ;
	strcpy( jsonPaymentSection->currencyCode , "CHF" ) ;
	strcpy( jsonPaymentSection->tip , "10.01" ) ;

	strcpy( jsonPaymentSection->payloadName[0] , "TST0" ) ;
	strcpy( jsonPaymentSection->payloadData[0] , "TestingData0" ) ;
	strcpy( jsonPaymentSection->payloadName[1] , "TST1" ) ;
	strcpy( jsonPaymentSection->payloadData[1] , "TestingData1" ) ;
	strcpy( jsonPaymentSection->payloadName[2] , "TST2" ) ;
	strcpy( jsonPaymentSection->payloadData[2] , "TestingData2" ) ;

	jsonPaymentSection->payloadSize = 3 ;

	strcpy( jsonPaymentSection->userName , "merchant" ) ;
	strcpy( jsonPaymentSection->passWord , "merchant" ) ;
	strcpy( jsonPaymentSection->vtID , "1" ) ;

	strcpy( jsonPaymentSection->postBackUri , TOKEN_URL_FX_REG_SERVICE_AUTH ) ;

	memset( jsBuffer , 0 , sizeof( bufLen ) ) ;

	for( elemListCtr = 0 ; elemListCtr < maxElemList ; elemListCtr++ )
	{
		for( bufMapList = 0 ; bufMapList < hostDataListSizePymt ; bufMapList++ )
		{
			if( strcmp( elemList[elemListCtr].elementID , TOKEN_PRIMID_PAYLOAD ) == 0 )
			{
				jsonPayloadWrap( &g_dataContainer , jsBuffer ) ;
				bufMapList = hostDataListSizePymt ;
			}
			else if( strcmp( elemList[elemListCtr].elementID , hostDataListPymt[bufMapList].elementID ) == 0 )
			{
				jsonAddElement( hostDataListPymt[bufMapList].elementID , hostDataListPymt[bufMapList].value , jsBuffer ) ;
				bufMapList = hostDataListSizePymt ;
			}
		}
	}

	PrnInit();
	PrnStr( jsBuffer ) ;
	PrnStart() ;

	//jsonPackageWrap( jsBuffer , jsBuffer ) ;
	/*
	PrnInit();
	PrnStr( jsBuffer ) ;
	PrnStart() ;
	*/
	return retVal ;

}

/*****************************************************************************
*
*  Bluetooth.c - D200/D210 Demo bluetooth driver Code.
*  Copyright (C) 2012 PAXSZ - http://www.paxsz.com.cn/
*  Author:zhaorh Email:zhaorh@paxsz.com
*
*****************************************************************************/

#include "bluetooth.h"
#include <posapi.h>
#include <Stdarg.h>
#include <Stdlib.h>
#include <Stdio.h>
#include <String.h>

#define DEBUG_BT

int giBtOpened = 0;
ST_BT_CONFIG gszBtConfig;
uchar gucBtStatus[7];
int s_GetSlave(const uchar *SlaveInfo, uchar * Addr,uchar * name);
int s_CheckBaudOk(uchar *baud);
int s_StringToMac(char *MacString, uchar *mac);
int s_CheckMacOk(uchar *Mac);
int s_CheckBaudOk(uchar *baud);
int BtOpen(void)
{
	uchar ucRet;
	uchar ComParam[1024];
	int BaudRate;
	int iRet;
	int i = 0;
	if (giBtOpened == 1)
	{
		return BT_RET_ERROR_NOTCLOSE;
	}
	ucRet = PortOpen(COM_BT,"230400,8,n,1");
	if (ucRet != 0) return BT_RET_ERROR_PORTERROR;
	giBtOpened = 1;
	memset(&gszBtConfig, 0, sizeof(gszBtConfig));
	BtGetStatus(gucBtStatus);
	BtGetConfig(&gszBtConfig);
	return BT_RET_OK;
}
int BtClose(void)
{
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	PortClose(COM_BT);
	giBtOpened = 0;
	return BT_RET_OK;
}

int BtSetName(uchar *Name)
{
	uchar ATstrIn[100] = "";
	uchar Resp[100];
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	if (Name == NULL) return -2;
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0] == BT_STATE_CONNECT)
	{
		return BT_RET_ERROR_DATAMODE;
	}
	if (strlen(Name) > BT_NAME_MAXLEN) return BT_RET_ERROR_PARAMERROR;
	memset(Resp, 0, sizeof(Resp));

	sprintf(ATstrIn, "\rSET BT NAME %s\r",Name);
	BtSendCmd(ATstrIn, Resp,sizeof(Resp),1000,0,2);
	
	memset(gszBtConfig.name, 0, sizeof(gszBtConfig.name));
	strcpy(gszBtConfig.name, Name);
	return BT_RET_OK;

}
int BtSetRole(int role)
{
	uchar ATstrIn[100] = "";
	uchar Resp[100];
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0] == BT_STATE_CONNECT)
	{
		return BT_RET_ERROR_DATAMODE;
	}
	if (role != BT_ROLE_MASTER && role != BT_ROLE_SLAVE) return BT_RET_ERROR_PARAMERROR;

	gszBtConfig.role = role;
	return BT_RET_OK;
	
}
int BtSetPin(uchar *Pin)
{
	uchar ATstrIn[100] = "";
	uchar Resp[100];
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0] == BT_STATE_CONNECT)
	{
		return BT_RET_ERROR_DATAMODE;
	}
	if (Pin == NULL) return BT_RET_ERROR_NULL;
	if (strlen(Pin) > BT_PIN_MAXLEN) return BT_RET_ERROR_PARAMERROR;
	memset(Resp, 0, sizeof(Resp));

	sprintf(ATstrIn, "\rSET BT AUTH * %s\r",Pin);
	BtSendCmd(ATstrIn, Resp,sizeof(Resp),1000,0,2);
	memset(gszBtConfig.pin, 0, sizeof(gszBtConfig.pin));
	strcpy(gszBtConfig.pin, Pin);
	return BT_RET_OK;
	
}

int BtGetConfig(ST_BT_CONFIG *ConfigInfo)
{
	uchar *pStr1=NULL, *pStr2=NULL;
	int role;//主从模式
	uchar name[STRING_LEN];//设备名
	uchar pin[STRING_LEN];//设备PIN
	uchar baud[STRING_LEN];//波特率
	uchar mac[BT_MAC_LEN];//本机mac
	int baudrate;
	uchar ATstrIn[100];
	uchar Resp[2048];
	uchar temp[1024];
	ST_BT_CONFIG TempBtConfig;
	int i;
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	if (ConfigInfo == NULL)
	{
		return BT_RET_ERROR_NULL;
	}
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0]== BT_STATE_CONNECT)
	{
		ConfigInfo->baud = gszBtConfig.baud;
		strcpy(ConfigInfo->name, gszBtConfig.name);
		strcpy(ConfigInfo->pin, gszBtConfig.pin);
		strcpy(ConfigInfo->mac, gszBtConfig.mac);
		ConfigInfo->role = gszBtConfig.role;
		return BT_RET_OK;
	}
	memset(ATstrIn, 0, sizeof(ATstrIn));
	memset(Resp, 0, sizeof(Resp));
	memset(temp, 0, sizeof(temp));

	sprintf(ATstrIn, "\rSET\r");
	BtSendCmd(ATstrIn, Resp,sizeof(Resp),5000,10,19);
	
	//本机MAC
	if (strstr(Resp, "SET BT BDADDR ") == NULL) return BT_RET_ERROR_EXPLAINERROR;
	pStr1 = strstr(Resp, "SET BT BDADDR ");
	pStr1 += strlen("SET BT BDADDR ");
	pStr2 = strstr(pStr1, "\r\n");
	if (pStr2 == NULL) return BT_RET_ERROR_EXPLAINERROR;
	if (pStr2 - pStr1 >= BT_MAC_LEN) return BT_RET_ERROR_EXPLAINERROR;//MAC字符过长
	memset(mac, 0, sizeof(mac));
	memcpy(mac, pStr1, pStr2 - pStr1);
	
	//设备名
	if (strstr(Resp, "SET BT NAME") == NULL) return BT_RET_ERROR_EXPLAINERROR;
	pStr1 = strstr(Resp, "SET BT NAME ");
	pStr1 += strlen("SET BT NAME ");
	pStr2 = strstr(pStr1, "\r\n");
	if (pStr2 == NULL) return BT_RET_ERROR_EXPLAINERROR;
	if (pStr2 - pStr1 > BT_NAME_MAXLEN) return BT_RET_ERROR_EXPLAINERROR;//设备名字符过长
	memset(name, 0, sizeof(name));
	memcpy(name, pStr1, pStr2 - pStr1);

	
	//设备PIN
	if (strstr(Resp, "SET BT AUTH * ") == NULL)
	{
		memset(ATstrIn, 0, sizeof(ATstrIn));
		memset(Resp, 0, sizeof(Resp));
		sprintf(ATstrIn, "\rSET BT AUTH * 0000\r");
		BtSendCmd(ATstrIn, Resp,sizeof(Resp),100,10,2);//无密码时设置默认密码为0000
		memset(pin, 0, sizeof(pin));
		memcpy(pin, "0000", 4);
	}
	else
	{
		pStr1 = strstr(Resp, "SET BT AUTH * ");
		pStr1 += strlen("SET BT AUTH * ");
		pStr2 = strstr(pStr1, "\r\n");
		if (pStr2 == NULL) return BT_RET_ERROR_EXPLAINERROR;
		if (pStr2 - pStr1 > BT_PIN_MAXLEN) return BT_RET_ERROR_EXPLAINERROR;//密码字符过长
		memset(pin, 0, sizeof(pin));
		memcpy(pin, pStr1, pStr2 - pStr1);
	}
	
	

	//波特率
	if (strstr(Resp, "SET CONTROL BAUD ") == NULL) return BT_RET_ERROR_EXPLAINERROR;
	pStr1 = strstr(Resp, "SET CONTROL BAUD ");
	pStr1 += strlen("SET CONTROL BAUD ");
	pStr2 = strstr(pStr1, ",");
	if (pStr2 == NULL) return BT_RET_ERROR_EXPLAINERROR;
	memset(temp, 0, sizeof(temp));
	memcpy(temp, pStr1, pStr2 - pStr1);
	baudrate = atoi(temp);

	memset(&TempBtConfig, 0, sizeof(ST_BT_CONFIG));
	TempBtConfig.role = gszBtConfig.role;
	strcpy(TempBtConfig.name, name);
	strcpy(TempBtConfig.pin, pin);
	TempBtConfig.baud = baudrate;
	s_StringToMac(mac, TempBtConfig.mac);
	
	memcpy(ConfigInfo, &TempBtConfig, sizeof(TempBtConfig));
	DelayMs(100);
	PortReset(COM_BT);
	return BT_RET_OK;
}

int BtScan(ST_BT_SLAVE *outSlave, int *SlaveCount, int TimeOutMs)
{
	uchar ATstrIn[100];
	uchar Resp[4096];
	uchar *pStr1=NULL, *pStr2=NULL,*pStr3=NULL;
	uchar *pAddr1=NULL, *pAddr2=NULL;
	uchar temp[1024];
	int count;
	int i = 0;
	int InquiryTime;
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0] == BT_STATE_CONNECT)
	{
		return BT_RET_ERROR_DATAMODE;
	}
	if (outSlave == NULL || SlaveCount == NULL)
	{
		return BT_RET_ERROR_NULL;
	}
	if (TimeOutMs < 6000 || TimeOutMs > 60000) return BT_RET_ERROR_PARAMERROR;
	memset(ATstrIn, 0, sizeof(ATstrIn));
	memset(Resp, 0, sizeof(Resp));
	if (gszBtConfig.role != BT_ROLE_MASTER)
	{
		return BT_RET_ERROR_ROLEERROR;
	}
	InquiryTime = (TimeOutMs-5000) / 1280 + 1;
	
	sprintf(ATstrIn, "\rINQUIRY %d NAME\r",InquiryTime);
	BtSendCmd(ATstrIn, Resp,sizeof(Resp),TimeOutMs,0,60);

	if (strstr(Resp, "NAME ") == NULL)
	{
		*SlaveCount = 0;
		return BT_RET_OK;
	}
	pStr1 = strstr(Resp, "NAME ");
	count = 0;
	while(1)
	{
		pStr2 = strstr(pStr1,"\r\n");
		if (pStr2 == NULL)
		{
			break;
		}
		pStr2 += strlen("\r\n");
		memset(temp, 0, sizeof(temp));
		memcpy(temp, pStr1, pStr2 - pStr1);

		memset(outSlave[count].addr, 0, STRING_LEN);
		memset(outSlave[count].name, 0, STRING_LEN);
		if (s_GetSlave(temp, outSlave[count].addr, outSlave[count].name) == 0)
		{
			count++;
		}
		pStr1 = strstr(pStr2, "NAME ");
		if (pStr1 == NULL)
		{
			break;
		}
	}
	
	*SlaveCount = count;
	return BT_RET_OK;
	
}
int BtConnect(ST_BT_SLAVE Slave)
{
	uchar ATstrIn[20];
	uchar Resp[1024];
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0] == BT_STATE_CONNECT)
	{
		return BT_RET_ERROR_DATAMODE;
	}
	if (strlen(Slave.addr) != strlen("00:00:00:00:00:00")) return BT_RET_ERROR_PARAMERROR;
	if (s_CheckMacOk(Slave.addr) != 0) return BT_RET_ERROR_PARAMERROR;
	memset(ATstrIn, 0, sizeof(ATstrIn));
	memset(Resp, 0, sizeof(Resp));
	if (gszBtConfig.role != BT_ROLE_MASTER)
	{
		return BT_RET_ERROR_ROLEERROR;
	}
	//连接
	sprintf(ATstrIn, "\rCALL %s 1101 RFCOMM\r",Slave.addr);
	BtSendCmd(ATstrIn, Resp,sizeof(Resp),5000,0,2);
	return BT_RET_OK;
}
int BtDiscon(void)
{
	uchar ATstrIn[20];
	uchar Resp[1024];
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0] == BT_STATE_CONNECT)
	{
		DelayMs(1000);
		PortSends(COM_BT, "+++", 3);
		DelayMs(1000);
	}
	sprintf(ATstrIn, "\rCLOSE 0\r");
	BtSendCmd(ATstrIn, Resp,sizeof(Resp),1000,0,2);
	return BT_RET_OK;
}
int BtSendCmd(const uchar * ATstrIn,
               uchar *RspOut,
               ushort Rsplen,
               ushort TimeOut, 
               ushort Mode,
			   ushort ExpectCount)
{
	uchar ucRet;
	int iRet;
	int len = 0;
	int i = 0;
	int count = 0;
	int ModeStarted = 0;
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0] == BT_STATE_CONNECT)
	{
		return BT_RET_ERROR_DATAMODE;
	}
	if (ATstrIn != NULL)
	{
		PortReset(COM_BT);
		ucRet = PortSends(COM_BT,(uchar *)ATstrIn,strlen(ATstrIn));
		if (ucRet != 0)
		{
			return BT_RET_ERROR_PORTERROR;
		}
	}
	
	if (RspOut == NULL || Rsplen == 0 || ExpectCount == 0)
	{
		return BT_RET_OK;
	}
	TimerSet(TIME_BT_0, (TimeOut+99) / 100);
	while(1)
	{
		if (TimerCheck(TIME_BT_0) == 0)
		{
			return BT_RET_OK;
		}
		iRet = PortRecv(COM_BT, RspOut+len, 0);
		if(iRet == 0)
		{
			if (len >= Rsplen) return BT_RET_OK;
			len++;
			if (RspOut[len-1] == '\n')
			{
				count++;
				if (count >= ExpectCount)
				{
					if (Mode == 0)
					{
						return BT_RET_OK;
					}
					else
					{
						TimerSet(TIME_BT_1, (Mode+99) / 100);
						ModeStarted = 1;
					}
				}
				
			}
		}
		if (Mode > 0 && ModeStarted == 1)
		{
			if (TimerCheck(TIME_BT_1) == 0)
			{
				return BT_RET_OK;
			}
		}
		
	}
	
}

int BtResetSetting(void)
{
	uchar ATstrIn[100];
	uchar Resp[1024];
	int iRet;
	if (giBtOpened == 0)
	{
		return BT_RET_ERROR_NOTOPEN;
	}
	BtGetStatus(gucBtStatus);
	if (gucBtStatus[0] == BT_STATE_CONNECT)
	{
		return BT_RET_ERROR_DATAMODE;
	}
	memset(ATstrIn, 0, sizeof(ATstrIn));
	memset(Resp, 0, sizeof(Resp));
	sprintf(ATstrIn, "\rSET RESET\r");
	iRet = BtSendCmd(ATstrIn, Resp,sizeof(Resp),500,0,2);
	if (iRet) return BT_RET_ERROR_EXPLAINERROR;
	DelayMs(1000);
	PortReset(COM_BT);
	BtClose();
	return BtOpen();
}
int s_GetSlave(const uchar *SlaveInfo, uchar * Addr,uchar * name)
{
	uchar *pStr1, *pStr2;
	uchar temp[1024];
	if (strstr(SlaveInfo,"NAME ") == NULL)
	{
		return BT_RET_ERROR_DEFAULT;
	}
	memset(temp, 0, sizeof(temp));
	pStr1 = strstr(SlaveInfo, "NAME ") + strlen("NAME ");
	pStr2 = strstr(pStr1, " ");
	if (pStr2 == NULL || pStr2 <= pStr1) return BT_RET_ERROR_DEFAULT;
	memcpy(temp, pStr1, pStr2-pStr1);
	if (strlen(temp) != strlen("00:00:00:00:00:00")) return BT_RET_ERROR_DEFAULT;
	memcpy(Addr, temp, strlen(temp));
	
	pStr1 =pStr2 + strlen(" ");
	pStr1 = strstr(pStr1, "\"");
	if (pStr1 == NULL) return BT_RET_ERROR_DEFAULT;
	pStr1 += strlen("\"");
	pStr2 = strstr(pStr1, "\"");
	if (pStr2 == NULL || pStr2 <= pStr1) return BT_RET_ERROR_DEFAULT;
	memset(temp, 0, sizeof(temp));
	memcpy(temp, pStr1, pStr2-pStr1);
	if (strlen(temp) > BT_NAME_MAXLEN) return BT_RET_ERROR_DEFAULT;
	memcpy(name, temp, strlen(temp));
	
	return BT_RET_OK;
}
int s_CheckBaudOk(uchar *baud)
{
	int i;
	for (i = 0;i < strlen(baud);i++)
	{
		if (baud[i] < '0' || baud[i] > '9') break; 
	}
	if (i != strlen(baud)) return BT_RET_ERROR_DEFAULT;//波特率字符非法
	return BT_RET_OK;
}
int s_CheckMacOk(uchar *Mac)
{
	int i;
	int maclen = strlen("00:00:00:00:00:00");
	for (i = 0;i < maclen;i++)
	{
		if (i % 3 == 2)
		{
			if (Mac[i] != ':') return BT_RET_ERROR_DEFAULT;
		}
		else
		{
			if ((Mac[i] < '0' || Mac[i] > '9') && (Mac[i] < 'a' || Mac[i] > 'f') && (Mac[i] < 'A' || Mac[i] > 'F'))
			{
				return BT_RET_ERROR_DEFAULT;
			}
		}
	}
	return BT_RET_OK;
}
int s_StringToHex(char *HexString, uchar *hex)
{
	uchar result = 0;
	if (HexString[0] >= '0' && HexString[0] <= '9')
	{
		result = (HexString[0] - '0') * 16;
	}
	else if (HexString[0] >= 'a' && HexString[0] <= 'f')
	{
		result = (HexString[0] - 'a' + 10) * 16;
	}
	else if (HexString[0] >= 'A' && HexString[0] <= 'F')
	{
		result = (HexString[0] - 'A' + 10) * 16;
	}
	else
	{
		return -1;
	}
	if (HexString[1] >= '0' && HexString[1] <= '9')
	{
		result += HexString[1] - '0';
	}
	else if (HexString[1] >= 'a' && HexString[1] <= 'f')
	{
		result += HexString[1] - 'a' + 10;
	}
	else if (HexString[1] >= 'A' && HexString[1] <= 'F')
	{
		result += HexString[1] - 'A' + 10;
	}
	else
	{
		return -1;
	}
	*hex = result;
	return 0;
}
int s_StringToMac(char *MacString, uchar *mac)
{
	int i = 0;
	uchar tempchar;
	char temp[3];
	char *pStr1 = NULL, *pStr2 = NULL;
	pStr1 = MacString;
	for (i = 0;i < 5;i++)
	{
		pStr2 = strstr(pStr1, ":");
		if (pStr2 == NULL || ((pStr2-pStr1) != 2)) return -1;
		memset(temp, 0, sizeof(temp));
		memcpy(temp, pStr1, pStr2-pStr1);
		if(s_StringToHex(temp, &tempchar) == -1) return -1;
		mac[i] = tempchar;
		pStr1 = pStr2 + strlen(":");
	}
	memset(temp, 0, sizeof(temp));
	memcpy(temp, pStr1, 2);
	if(s_StringToHex(temp, &tempchar) == -1) return -1;
	mac[5] = tempchar;
	return 0;
}

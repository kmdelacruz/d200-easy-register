#ifndef _UTILS_H
#define _UTILS_H

int AscToHex( unsigned char * convertBuffer , unsigned char * dataBuffer ,  int convertMaxLen , int dataLen , int * actualLen ) ;
int HexToAsc( unsigned char * convertBuffer , unsigned char * dataBuffer ,  int convertMaxLen , int dataLen , int * actualLen ) ;

int AscToLng( unsigned char * dataBuffer , int dataLen , long * result ) ;
int LngToAsc( long data , unsigned char *result ) ;

long Pow( int base , int power ) ;

int DisplayHexBuffer( unsigned char * dataBuffer , int dataLen ) ;

#endif
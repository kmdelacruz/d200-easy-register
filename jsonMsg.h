#ifndef  _JSON_MSG_H
#define  _JSON_MSG_H

#include <jsmn.h>
#include "global.h"

#define TOKEN_PRIMID_COMMAND							"commandType"
#define TOKEN_PRIMID_PAYLOAD							"payload"
#define	TOKEN_PRIMID_POSTBACKURI						"postbackUri"
#define TOKEN_PRIMID_MESSAGE							"message"
#define	TOKEN_PRIMID_KSN								"ksn"
#define	TOKEN_PRIMID_SESSIONID							"sessionId"
#define TOKEN_PRIMID_PAYMENTREQUEST						"paymentRequest"

#define TOKEN_PROCID_TRANSMIT							"TRANSMIT"
#define TOKEN_PROCID_PROMPTUSER							"PROMPT_USER"
#define TOKEN_PROCID_ONLINEPROC							"ONLINE_PROCESSING"
#define	TOKEN_PROCID_REJECT								"REJECT"
#define	TOKEN_PROCID_COMPLETE							"COMPLETE"

#define TOKEN_EMVSTEP_INIT								"INIT_PATH"
#define	TOKEN_EMVSTEP_PSEDDF							"PSE_DDF_PATH"
#define	TOKEN_EMVSTEP_PSERECORD							"PSE_RECORD_PATH"
#define	TOKEN_EMVSTEP_CANDLIST							"CANDIDATE_LIST_PATH"
#define	TOKEN_EMVSTEP_APPSELECT							"SELECT_APP_PATH"
#define TOKEN_EMVSTEP_AIP								"AIP_AFL_PATH"
#define TOKEN_EMVSTEP_READRECORD						"APP_DATA_PATH"
#define	TOKEN_EMVSTEP_FIRSTGENAC						"CARD_ACTION_ANALYSIS_1_PATH"
#define	TOKEN_EMVSTEP_SECONDGENAC						"CARD_ACTION_ANALYSIS_2_PATH"
#define	TOKEN_EMVSTEP_ISSUERPROC						"HOST_RESPONSE_PATH"

#define	TOKEN_BASE_URL									"http://registry.colixo.info:8787"
#define TOKEN_URL_FX_REGISTRY							"/session"
#define TOKEN_URL_FX_GET_MERCHANT_PROFILE				"/merchants"
#define TOKEN_URL_FX_TERMINAL_ACTION_ANALYSIS			"/emv/terminal-action-analysis"
#define TOKEN_URL_FX_ADD_TRANSACTION					"/credit/sales"
#define TOKEN_URL_FX_UPLOAD_SIGNATURE					"/signatures"
#define TOKEN_URL_FX_UPLOAD_PRODUCTS					"/images/products"
#define TOKEN_URL_FX_GET_TXN_HISTORY					"/credit/transaction/"
#define TOKEN_URL_FX_CHANGE_PASSWORD					"/session/"

#define TOKEN_URL_FX_LOAD_KEYS							"/emv/load-keys"
#define TOKEN_URL_FX_EMV_SELECT_PSE						"/emv/branches/"
#define TOKEN_URL_FX_EMV_SELECT_PSE_RESPONSE			"/emv/select-pse-response"
#define TOKEN_URL_FX_EMV_READ_PSE_RECORD_RESPONSE		"/emv/read-pse-response"
#define TOKEN_URL_FX_EMV_SEND_AID_LIST_RESPONSE			"/emv/aid-list-response"
#define TOKEN_URL_FX_EMV_SELECT_APP_RESPONSE			"/emv/select-app-response"
#define TOKEN_URL_FX_EMV_GPO_RESPONSE					"/emv/gpo-response"
#define TOKEN_URL_FX_EMV_READ_APP_RESPONSE				"/emv/read-app-response"
#define TOKEN_URL_FX_EMV_COMPLETE_TXN					"/emv/confirm-transaction"
#define TOKEN_URL_FX_REG_SERVICE_AUTH					"/registry/service/auth"

#define PYMTBRKR_FIELD_TIMESTAMP						"requestTimestamp"
#define	PYMTBRKR_FIELD_CURRENCYCODE						"currencyCode"
#define	PYMTBRKR_FIELD_TRACK2DATA						"track2Data"
#define	PYMTBRKR_FIELD_AID								"aid"
#define	PYMTBRKR_FIELD_EMVCHIPDATA						"EMVChipData"
#define PYMTBRKR_FIELD_TIP								"tip"
#define	PYMTBRKR_FIELD_POSCONDCODE						"POSConditionCode"
#define	PYMTBRKR_FIELD_ENTRYMODE						"entryMode"
#define	PYMTBRKR_FIELD_AMOUNT							"amount"
#define	PYMTBRKR_FIELD_TC								"tc"
#define	PYMTBRKR_FIELD_LONGITUDE						"longitude"
#define	PYMTBRKR_FIELD_LATITUDE							"latitude"
#define	PYMTBRKR_FIELD_PANTOKEN							"panToken"
#define	PYMTBRKR_FIELD_CARDSEQNUM						"cardSequenceNumber" 
#define PYMTBRKR_FIELD_TXNAMOUNT						"transactionAmount"
#define	PYMTBRKR_FIELD_AIDSCHEME						"aidScheme" 
#define	PYMTBRKR_FIELD_APPROVALCODE						"approvalCode"
#define	PYMTBRKR_FIELD_BATCHID							"batchId"
#define	PYMTBRKR_FIELD_BRANCHID							"branchId"
#define	PYMTBRKR_FIELD_CARDHOLDERNAME					"cardholderName"
#define	PYMTBRKR_FIELD_ID								"id"
#define	PYMTBRKR_FIELD_INVOICENUM						"invoiceNumber"
#define PYMTBRKR_FIELD_MASKEDPAN						"maskedPan"
#define	PYMTBRKR_FIELD_MID								"mid"
#define	PYMTBRKR_FIELD_REFNO							"refNo"
#define	PYMTBRKR_FIELD_RQSTTIMESTAMP					"requestTimestamp"
#define	PYMTBRKR_FIELD_RESPONSECODE						"responseCode"
#define	PYMTBRKR_FIELD_STATUS							"status"
#define	PYMTBRKR_FIELD_TC								"tc"
#define	PYMTBRKR_FIELD_TID								"tid"
#define	PYMTBRKR_FIELD_STAN								"traceNo"
#define	PYMTBRKR_FIELD_TRANSACTIONTYPE					"transactionType"

#define REGISTER_FIELD_USERNAME							"usernamekahitano"
#define REGISTER_FIELD_PASSWORD							"password"
#define	REGISTER_FIELD_VTID								"vtId"

typedef struct jsonBufferMappingElementList
{
	char * elementID ;
	char * value ;
	int valueSize ;
}s_jsonBufMapList ;

typedef struct jsonBuilderElementList
{
	char * elementID ;
}s_jsonBuildElemList ;

s_jsonBuildElemList hostBuildListPymt[DATACONTAINER_SIZE_64] ;
int hostBuildListSizePymt ;


s_jsonBufMapList hostDataListPymt[DATACONTAINER_SIZE_128];
s_jsonBufMapList hostDataListPrim[DATACONTAINER_SIZE_16] ;
s_jsonBufMapList hostDataListRgst[DATACONTAINER_SIZE_8] ;

int hostDataListSizePymt ;
int hostDataListSizePrim ;
int hostDataListSizeRgst ;



#define PROC_ID_MAX		5

//initializer functions
int jsonInitializerBuildPayment( void ) ;
int jsonInitializerBuildRegistration( void ) ;
int jsonInitializerParserPrimitives( void ) ;
int jsonInitializerParserPayment( void ) ;

int	jsonPymntBrkrParser( g_dataContainerBuffer * dataBuffer , const char * jsBuffer ) ;

int jsonEmvParser( g_dataContainerBuffer * dataBuffer , const char *jsBuffer ) ;

//builder functions
int jsonPackageWrap(char * valueBuffer , char * jsBuffer) ;
int jsonPrimitiveWrap( char * primitiveID , char * valueBuffer , char * jsBuffer ) ;
int jsonAddElement( char * primitiveID , char * valueBuffer , char * jsBuffer ) ;
int jsonBuildEmvSectionPayment( g_dataContainerBuffer * jsonPaymentSection , char * jsBuffer , int bufLen ) ;
int jsonBuildMessageGeneric( s_jsonBufMapList elemList[] , int maxElemList , g_dataContainerBuffer * jsonPaymentSection , char * jsBuffer , int bufLen ) ;

#endif
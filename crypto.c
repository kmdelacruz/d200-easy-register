#include <posapi.h>

#include <crypto.h>
#include <utils.h>

int WriteTerminalLoadingKey(uchar SourceKeyIndex, uchar SourceKeyType, 
			 uchar DestKeyIndex, uchar DestKeyType, 
			 uchar *pKeyValue, uchar KeyLen,
			 uchar *pKCV, int i)
{
	int iRet;
	
	ScrCls();
	ScrPrint(0, 4, 1, "Wrting...");
	DelayMs(3000);

	stKeyInfoIn.ucSrcKeyIdx  = SourceKeyIndex;
	stKeyInfoIn.ucSrcKeyType = SourceKeyType;
    stKeyInfoIn.ucDstKeyIdx  = DestKeyIndex;
	stKeyInfoIn.ucDstKeyType =  DestKeyType;
   
	stKeyInfoIn.iDstKeyLen = KeyLen;
	memcpy(stKeyInfoIn.aucDstKeyValue, pKeyValue, KeyLen);
	
	if (pKCV == NULL)
	{
		stKcvInfoIn.iCheckMode = 0;
	}

	iRet = PedWriteKey(&stKeyInfoIn, &stKcvInfoIn);
	if (iRet != 0)
	{
		ScrPrint(0,0,0,"WriteKey:%d",iRet);
		ScrPrint(0, 4, 1, "Write TLK Error");
		getkey();
		return -1;
	}
	g_uckeyNum[i] = KeyLen;

	ScrCls();
	ScrPrint(0, 2, 1, "Write Key Success");
	DelayMs(3000);

	return 0;
}

void TerminalLoadingKey()
{
	int iRet=0;
	unsigned char ucRet;
	unsigned char ucBuf[KEY_LENGTH + 1];

	memset(&g_uckeyNum, 0, sizeof(g_uckeyNum));

	ScrCls();
	ScrPrint(0, 2, 1, "Input TLK:\n");
	
	iRet = WriteTerminalLoadingKey(0, PED_BKLK, 1, PED_BKLK, (unsigned char*)"1234567890123456" , KEY_LENGTH, (unsigned char*)NULL, 0);

	if (iRet != 0)
	{
		ScrPrint(0,0,0,"WriteKey:%d",iRet);
		ScrPrint(0, 4, 1, "Write TLK Error");
		getkey();
	}
}

void WriteTerminalInitialKey(ST_KCV_INFO *stKVCInfo)
{
	uchar ucRet;
	uchar ucBuf[KEY_LENGTH + 1];
	/*
	if (g_uckeyNum[0] == 0)
	{
		ScrCls();
		ScrPrint(0, 4, 1,"   No TLK   ");
		DelayMs(3000);
		return;
	}
	*/
	memset(&g_uckeyNum[2], 0, sizeof(&g_uckeyNum[2]));
	
	if (stKVCInfo == NULL)
	{
		stKcvInfoIn.iCheckMode = 0;
	}

	ScrCls();
	ScrPrint(0, 2, 1, "Input TIK:\n");
	//ucRet = PedWriteTIK(1, 1, KEY_LENGTH, (unsigned char*)"6543210987654321", (unsigned char*)KSN_VALUE_IN, &stKcvInfoIn);
	ucRet = PedWriteTIK(1, 1, KEY_LENGTH, (unsigned char*)"82DF8AC0229162AF040CF4D076437279", (unsigned char*)KSN_VALUE_IN, &stKcvInfoIn);

	if (ucRet != 0)
	{
		ScrCls();
		ScrPrint(0, 4, 1, "Write TIK Error");
		getkey();
		return;
	}
	
	g_uckeyNum[2] = KEY_LENGTH;
	ScrCls();
	ScrPrint(0, 4, 1, "Write TIK Success");
	DelayMs(3000);
}

void DESProcess( void )
{
	 unsigned char testing[64] ;
	 unsigned char ksn[16] ;
	 memset( testing , 0 , sizeof( testing ) ) ;
	 memset( ksn , 0 , sizeof( ksn ) ) ;

	 ScrCls() ;
	 Lcdprintf("DESP\n:%d",  PedDukptDes( 1, 1, (unsigned char*)"00000000", 32, (unsigned char*)"1234567890ABCDEF1234560987FEDCBA" , testing, ksn , 3 ) ) ; 
	 getkey() ;
	 
	 DisplayHexBuffer( ksn , 32 ) ;
	 DisplayHexBuffer( ksn , 10 ) ;

}

void testEncryption( void )
{
	unsigned char ksn[12] ;

	//TerminalLoadingKey() ;
	WriteTerminalInitialKey( NULL ) ;

	memset( ksn, 0 , sizeof( ksn ) ) ;
	ScrCls() ;
	Lcdprintf( "getDukptKsn:%d" , PedGetDukptKSN( 1 , ksn ) ) ;
	getkey() ;
	ScrCls() ;

	DisplayHexBuffer( ksn , 10 ) ;

	DESProcess() ;
}

//#include <posapi_all.h>
//#include <posapi_s80.h>
#include <posapi.h>
#include <apduCmd.h>
#include <utils.h>

void apduTestFunc( void )
{
	return;
}

void iccStartCmd( void )
{
	unsigned char buffer [64] ;
	int retVal ;
	memset( buffer , 0, sizeof( buffer ) ) ;
    IccInit( 0 , buffer ) ;
	IccAutoResp( 0, 1 ) ;
}

void iccStopCmd( void )
{
	unsigned char buffer [64] ;
	int retVal ;
	memset( buffer , 0, sizeof( buffer ) ) ;
	IccClose( 0 ) ;
}

int iccDetectCard( void )
{
	return (int)IccDetect( 0 ) ;
}

void iccSendCmd( unsigned char * dataBuffer , int dataBufferLen , unsigned char * dataOutput , int * dataOutputLen ) 
{
	APDU_SEND apduBuffer ;
	APDU_RESP apduResponse ;

	int cmdLen ;
	long lnBuf ;

	unsigned char tempBuf[256] ;

	char buf[8] ;

	
	memcpy( apduBuffer.Command , dataBuffer , 4 ) ;
	//memcpy( apduBuffer.Lc , dataBuffer+4 , 1 ) ;
	//apduBuffer.Lc = dataBuffer[4]+dataBuffer[5] ;
	apduBuffer.Lc = dataBuffer[4] ;
	memcpy( apduBuffer.DataIn , dataBuffer+5 , apduBuffer.Lc ) ;
	apduBuffer.Le = 0 ;

	/*
	memset( &apduBuffer , 0 , sizeof( APDU_SEND ) ) ;
	memset( &apduResponse , 0 , sizeof( APDU_RESP ) ) ;

	memset( buf , 0 , sizeof( buf ) ) ; 
	LngToAsc( 234 , (unsigned char*)buf ) ;

	AscToHex( apduBuffer.Command , dataBuffer , 4 , 8 , &cmdLen ) ;
	//AscToHex( apduBuffer.Lc , dataBuffer, 1 , 2 , &cmdLen ) ;
	AscToLng( dataBuffer+8 , 2 , &lnBuf ) ;
	apduBuffer.Lc = (unsigned short)lnBuf ;
	AscToHex( apduBuffer.DataIn , dataBuffer+10 , 512 , lnBuf*2 , &cmdLen ) ;
	AscToLng( dataBuffer+10+lnBuf*2 , 2 , &lnBuf ) ;
	apduBuffer.Le = lnBuf ;
	*/
	
	ScrCls();
	Lcdprintf( "Cmd:%02x %02x %02x %02x\n" , apduBuffer.Command[0] , apduBuffer.Command[1] , apduBuffer.Command[2] , apduBuffer.Command[3] ) ;
		getkey();
	ScrCls();
	Lcdprintf( "Lc:%02x\n" , apduBuffer.Lc ) ;
		getkey();
	ScrCls();
	Lcdprintf( "Data:%02x %02x %02x %02x %02x %02x %02x %02x\n" , 
		apduBuffer.DataIn[0] , apduBuffer.DataIn[1] , apduBuffer.DataIn[2] , apduBuffer.DataIn[3] , 
		apduBuffer.DataIn[4] , apduBuffer.DataIn[5] , apduBuffer.DataIn[6] , apduBuffer.DataIn[7] ) ;
	getkey();
	ScrCls();
	Lcdprintf( "Le:%x\n" , apduBuffer.Le ) ;
		getkey();

	ScrCls();
	Lcdprintf( "Icc:%d\n" , IccIsoCommand( 0 , &apduBuffer , &apduResponse ) ) ;
	getkey();

	DisplayHexBuffer( apduResponse.DataOut , apduResponse.LenOut ) ;

	ScrCls();
	Lcdprintf( "Len:%d\n" , apduResponse.LenOut ) ;
	getkey();
	ScrCls();
	Lcdprintf( "SW A:%x\n" , apduResponse.SWA ) ;
	getkey();
	ScrCls();
	Lcdprintf( "SW B:%x\n" , apduResponse.SWB ) ;
	getkey();

	//Building the message output to ASC
	/*
	memset( tempBuf, 0 , sizeof( tempBuf ) ) ;
	LngToAsc( apduResponse.LenOut , tempBuf ) ;
	strcat( dataOutput , tempBuf ) ;

	memset( tempBuf, 0 , sizeof( tempBuf ) ) ;
	cmdLen = 0 ;
	HexToAsc( tempBuf , apduResponse.DataOut , sizeof( tempBuf ) , apduResponse.LenOut , &cmdLen ) ;
	strcat( dataOutput , tempBuf ) ;

	memset( tempBuf, 0 , sizeof( tempBuf ) ) ;
	cmdLen = 0 ;
	HexToAsc( tempBuf , &apduResponse.SWA , sizeof( tempBuf ) , 1 , &cmdLen ) ;
	strcat( dataOutput , tempBuf ) ;

	memset( tempBuf, 0 , sizeof( tempBuf ) ) ;
	cmdLen = 0 ;
	HexToAsc( tempBuf , &apduResponse.SWB , sizeof( tempBuf ) , 1 , &cmdLen ) ;
	strcat( dataOutput , tempBuf ) ;
	*/

	dataOutput[0] = apduResponse.SWA ;
	dataOutput[1] = apduResponse.SWB ;
	dataOutput[2] = apduResponse.LenOut / 256 ;
	dataOutput[3] = apduResponse.LenOut % 256 ;

	memcpy( dataOutput+4 , apduResponse.DataOut , apduResponse.LenOut ) ;
/*
	ScrCls();
	Lcdprintf( "Output:\n%s\n" , dataOutput ) ;
	getkey();
*/
	*dataOutputLen = apduResponse.LenOut+4 ;
	DisplayHexBuffer( dataOutput , apduResponse.LenOut+4 ) ;
}
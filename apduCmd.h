#ifndef _APDUCMD_H
#define _APDUCMD_H

void apduTestFunc( void ) ;

void iccStartCmd( void ) ;
void iccSendCmd( unsigned char * dataBuffer , int dataBufferLen , unsigned char * dataOutput , int * dataOutputLen ) ;
void iccStopCmd( void ) ;
int iccDetectCard( void ) ;

#endif

#include <posapi.h>
#include "utils.h"

int AscToHex( unsigned char * convertBuffer , unsigned char * dataBuffer ,  int convertMaxLen , int dataLen , int * actualLen ) 
{
	int dataBufLoop = 0 ;
	int convBufLoop = 0 ;
	int byteBuf ;
	int elevationFlag = 1 ;
	memset( convertBuffer, 0 , sizeof( convertBuffer ) ) ;

	for( dataBufLoop = 0 ; dataBufLoop <= dataLen && convBufLoop <= convertMaxLen; dataBufLoop++ )
	{

		if( dataBuffer[dataBufLoop] >= '0' && dataBuffer[dataBufLoop] <= '9' )
		{
			byteBuf = (int)( dataBuffer[dataBufLoop] - '0' );
		}
		else if( dataBuffer[dataBufLoop] >= 'A' && dataBuffer[dataBufLoop] <= 'F' )
		{
			byteBuf = (int)( dataBuffer[dataBufLoop] - 'A' ) + 10 ;
		}
		else
		{
			return RET_ERR ;
		}

		if( elevationFlag )
		{
			convertBuffer[convBufLoop] = (byteBuf<<4) ;
			elevationFlag = 0 ;
		}
		else
		{
			convertBuffer[convBufLoop] += byteBuf ;
			elevationFlag = 1 ;
			convBufLoop++ ;
		}
		/*
		ScrCls();
		ScrPrint( 0,0,0, "dataBufLoop:%d" , dataBuffer[dataBufLoop] ) ;
		ScrPrint( 0,1,0, "dataBuffer:%c" , dataBuffer[dataBufLoop] ) ;
		ScrPrint( 0,2,0, "byteBuf:%x" , byteBuf ) ;
		ScrPrint( 0,3,0, "convBufLoop:%d" , convBufLoop ) ;
		ScrPrint( 0,4,0, "convertBuffer:%x" , convertBuffer[convBufLoop-1] ) ;
		getkey();
		*/
	}

	*actualLen = convBufLoop ;
	return RET_OK ;
}

int HexToAsc( unsigned char * convertBuffer , unsigned char * dataBuffer ,  int convertMaxLen , int dataLen , int * actualLen ) 
{
	int dataBufLoop = 0 ;
	int convBufLoop = 0 ;
	int byteBuf ;
	int elevationFlag = 1 ;
	memset( convertBuffer, 0 ,convertMaxLen ) ;

	for( convBufLoop = 0 ; dataBufLoop < dataLen && convBufLoop <= convertMaxLen ; convBufLoop++ ) 
	{
		byteBuf = 0 ;
		if(elevationFlag)
		{
			if( dataLen >= 1 )
			{
				byteBuf = ( ( dataBuffer[dataBufLoop] & 0xF0 ) >> 4 ) ;
				elevationFlag = 0 ;
			}
		}
		else
		{
			byteBuf = ( dataBuffer[dataBufLoop] & 0x0F ) ;
			elevationFlag = 1 ;
			dataBufLoop++ ;
		}
		/*
		ScrCls();
		Lcdprintf( "1\n" ) ;
		Lcdprintf( "dataBufLoop:%d\n" , dataBufLoop ) ;
		Lcdprintf( "dataBuffer:%x\n" , dataBuffer[dataBufLoop] ) ;
		Lcdprintf( "byteBuf:%x\n" , byteBuf ) ;
		Lcdprintf( "byteBuf(d):%d\n" , byteBuf ) ;
		Lcdprintf( "convBufLoop:%d\n" , convBufLoop ) ;
		Lcdprintf( "convertBuffer:%c\n" , convertBuffer[convBufLoop] ) ;
		getkey();
		*/
		if( byteBuf >= 0 && byteBuf <= 9 )
		{
			convertBuffer[convBufLoop] = (char)(byteBuf + '0') ;
		}
		else if( byteBuf >= 10 && byteBuf <= 15 )
		{
			byteBuf -= 10 ;
			convertBuffer[convBufLoop] = (char)(byteBuf + 'A') ;		
		}
		else
		{
			return RET_ERR ;
		}
		/*
		ScrCls();
		Lcdprintf( "2\n" ) ;
		Lcdprintf( "dataBufLoop:%d\n" , dataBufLoop ) ;
		Lcdprintf( "dataBuffer:%x\n" , dataBuffer[dataBufLoop] ) ;
		Lcdprintf( "byteBuf:%x\n" , byteBuf ) ;
		Lcdprintf( "byteBuf(d):%d\n" , byteBuf ) ;
		Lcdprintf( "convBufLoop:%d\n" , convBufLoop ) ;
		Lcdprintf( "convertBuffer:%c\n" , convertBuffer[convBufLoop] ) ;
		getkey();
		*/

	}

	*actualLen = strlen( (const char*)convertBuffer ) ;
	/*
	ScrCls();
	Lcdprintf( "actualLen:%d\n" , *actualLen ) ;
	getkey() ;
	*/
	return RET_OK ;
}

int AscToLng( unsigned char * dataBuffer , int dataLen , long * result )
{
	int index = 0 ;
	int power = 0 ;
	int intBuf ;

	*result = 0 ;

	for( index = dataLen - 1 ; index >= 0 ; index-- )
	{
		if( dataBuffer[index] >= '0' && dataBuffer[index] <= '9' )
		{
			intBuf = (int)( dataBuffer[index] - '0' );
		}
		else if( dataBuffer[index] >= 'A' && dataBuffer[index] <= 'F' )
		{
			intBuf = (int)( dataBuffer[index] - 'A' ) + 10 ;
		}
		else
		{
			return RET_ERR ;
		}		
		//*result += ( intBuf * (16^power) ) ;
		*result += ( intBuf * Pow(16,power ) ) ;

		/*
		ScrCls();
		Lcdprintf("result:%ld\n" , *result ) ;
		Lcdprintf("intBuf:%d\n" , intBuf ) ;
		Lcdprintf("power:%d\n" , power ) ;
		Lcdprintf("16^0:%d\n" , 16^0 ) ;
		Lcdprintf("16^1:%d\n" , 16^1 ) ;
		Lcdprintf("pow:%d\n" , Pow(16,power )) ;
		getkey();
		*/
		power++ ;
	}

	return RET_OK ;
}

int LngToAsc( long data , unsigned char *result )
{
	int curIdx = 0 ;
	int pwrCtr = 1 ;
	long resultBuf ;

	for( pwrCtr = 1 ; pwrCtr >= 0 ; pwrCtr-- )
	{
		resultBuf = data / Pow(16, pwrCtr) ;
		if( resultBuf >= 10 )
		{
			result[curIdx] = ( resultBuf - 10 ) + 'A' ;

		}
		else if( resultBuf < 10 )
		{
			result[curIdx] = resultBuf + '0' ;
		}
		else
		{
			return RET_ERR ;
		}
		data -= Pow(16, pwrCtr)*resultBuf ;
		curIdx++ ;
	}	


		ScrCls();
		Lcdprintf("result:%s\n" , result ) ;
		getkey();

		return RET_OK; 

}


long Pow( int base , int power )
{
	int ctr = 1 ;
	long result = 1 ;
	while( ctr <= power )
	{
		result *= base ; 
		ctr++;
	}
	return result ;
}

int DisplayHexBuffer( unsigned char * dataBuffer , int dataLen ) 
{
	unsigned char buffer[32] ;
	int curLen = 0 ;
	int lineOutput = 1 ;
	int lineMaxChr = 0 ;
	unsigned char byteBuffer[8] ;
	//buffer = (unsigned char*)malloc( 24 ) ;

	memset( buffer , 0 , sizeof( buffer ) ) ;
	while( curLen <= dataLen )
	{
		ScrCls();
		Lcdprintf( "Hex Dump\n" ) ;

		lineOutput = 1 ;
		while( curLen <= dataLen && lineOutput <= 6 )
		{
			memset( buffer , 0 , sizeof( buffer ) ) ;
			lineMaxChr = 1  ;
			while( curLen <= dataLen && lineMaxChr <= 24 )
			{
				memset( byteBuffer , 0 , sizeof( byteBuffer ) ) ;
				sprintf( (char*)byteBuffer,"%02x ",dataBuffer[curLen] ) ; 
				curLen++ ;
				lineMaxChr+=3 ;
				strcat( (char*) buffer , (char*) byteBuffer ) ;
			}
			Lcdprintf( "%s\n" , buffer ) ;
			lineOutput++ ;
		}
		Lcdprintf( "cLen:%d , dataLen:%d" , curLen , dataLen ) ;
		getkey() ;
	}
	/*
	ScrPrint( 0,1,0, "dataBuffer:%x" , dataBuffer[dataBufLoop] ) ;
	ScrPrint( 0,2,0, "byteBuf:%x" , byteBuf ) ;
	ScrPrint( 0,2,0, "byteBuf(d):%d" , byteBuf ) ;
	ScrPrint( 0,3,0, "convBufLoop:%d" , convBufLoop ) ;
	ScrPrint( 0,4,0, "convertBuffer:%c" , convertBuffer[convBufLoop] ) ;
	*/

	//free( buffer ) ;
	return RET_OK ;
}
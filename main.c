/*****************************************************************************
*
*  Main.c - D200/D210 Demo main Code.
*  Copyright (C) 2012 PAXSZ - http://www.paxsz.com.cn/
*  Author:zhaorh Email:zhaorh@paxsz.com
*  Author:taomm  Email:taomm@paxsz.com
*
*****************************************************************************/
#include <posapi.h>
#include <stdarg.h>
#include "TradeDemo.h"
#include "apduCmd.h"
#include "crypto.h"

uchar gucTerminalInfo[30];
int wifiStatus;

int event_main(ST_EVENT_MSG *msg)
{
	
}

int main(void);
const APPINFO AppInfo={
	"Colixo D2xx Easy Register",
	"00000",
	"0.01",
	"KDELACRUZ-Colixo",
	"D2xxEzRegApp",
	"07042412000000",
	(ulong)main,
	(ulong)event_main,
	0,
	""};

void Trade_Demo(void);

int iStrXor(ulong ulInOutLen,uchar * pszInOut,ulong ulInLen,uchar * pszIn)
{
	int iLoop;
	if(ulInOutLen<ulInLen)
	{
		return -1;
	}
	for(iLoop=0;iLoop<ulInLen;iLoop++)
	{
		pszInOut[iLoop]=pszInOut[iLoop]^pszIn[iLoop];
	}
}

int main(void)
{	
	uchar ucFlag=0,ucKey,ucRet;
	uchar amount[20];
	int i=0;
	int fid=0;
	uchar logo[1024];
	uchar sendbuf[100];
	uchar recvbuf[100];
	int iRet = 0;
	uchar ch;
	int len;
    int step;
	uchar string[1024];
	uchar buf[100];
	
	memset(amount,0,sizeof(amount));
	memset(logo,0,1024);
	memset(string,0,1024);
	memset(buf,0,100);

    SystemInit();	
 	Beep();	
	PortOpen(0,"115200,8,n,1");

	memset(gucTerminalInfo, 0, sizeof(gucTerminalInfo));
	GetTermInfo(gucTerminalInfo);
	//ScrSetBgColor(RGB(0,0,0));	
    ScrCls();	

	
	Lcdprintf("Loading picture...\n");
	
	//iccStartCmd() ;
	//iccSendCmd( (unsigned char*)"00A4040007A000000004101000" , strlen( "00A4040007A000000004101000" ) , string ) ;
		//iccSendCmd( (unsigned char*)"00A4040007A000000003101000" , strlen( "00A4040007A0000000031010" ) ) ;

	//iccSendCmd( (unsigned char*)"00A404000E315041592E5359532E444446303100" , strlen( "00A404000E315041592E5359532E444446303100" ) , string ) ;

	//testEncryption() ;

    while(1)
	{
		fid = open("LoadPic",O_RDWR);//if the file is not exist,loading the png 
		ScrGotoxyEx(0,0);
		if(fid<0)
		{
			ucRet = LoadingPng();
			if(ucRet>0)
			{
				break;
			} 
		}
		else
		{
			break;
		}
	}

    while(1)
	{
			 
		DisplayAPPMenu();
	
	}	
	
    return 0;

}



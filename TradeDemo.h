
#ifndef TRADE_DEMO_H
#define TRADE_DEMO_H

#define PACKAGE_TRADE_AMOUNT
#define PACKAGE_SEND_RECEIPT
#define PACKAGE_PRINT_RECEIPT
#define PACKAGE_TRADE_OVER

#define ELE_NAME_MAX   100 
#define XML_DOC_MAX    8192
#define LANGUAGE "English"
//#define LANGUAGE "Russian"
//#define _DEBUG_
#define FILELINE __FILE__,__LINE__

extern  int  ScrSetBgColor(int bgColor);
#define RGB(r,g,b) ((int)((((WORD)((r&0xF8) >> 3)) << 11) | (((WORD)((g&0xFC) >> 2)) << 5) | ((uchar)((b&0xFC) >> 3))))
#define RGB_R(rgb) ((uchar)(((rgb)>>11) << 3))
#define RGB_G(rgb) ((uchar)((rgb)>>5) << 2)
#define RGB_B(rgb) ((uchar)((rgb) << 3))


#define	RECV_TIMEOUT  500

#define SIG_TO 60000


#ifndef WORD
#define WORD unsigned short
#endif


#define WHOLE_BG_COLOR      RGB(0x5F,0x9A,0xEB)//0x191970
#define TEXT_COLOR          0x0
#define COMMON_BUTTON_COLOR RGB(0xF4,0xF4,0xF4)//0x43CD80
#define CLEAR_COLOR         0x7CCD7C
#define ACCEPT_COLOR        0x7CCD7C



// Credit
#define CREDIT_LEFT     376    
#define CREDIT_TOP      55
#define CREDIT_RIGHT    CREDIT_LEFT + BUTTON_WIDTH
#define CREDIT_BOTTOM   CREDIT_TOP + BUTTON_HEIGHT

// Debit
#define DEBIT_LEFT      CREDIT_LEFT
#define DEBIT_TOP       CREDIT_BOTTOM + 10
#define DEBIT_RIGHT     DEBIT_LEFT + BUTTON_WIDTH
#define DEBIT_BOTTOM    DEBIT_TOP + BUTTON_HEIGHT

// EBT
#define EBT_LEFT        CREDIT_LEFT
#define EBT_TOP         DEBIT_BOTTOM + 14
#define EBT_RIGHT       EBT_LEFT + BUTTON_WIDTH
#define EBT_BOTTOM      EBT_TOP + BUTTON_HEIGHT

// Gift
#define GIFT_LEFT       CREDIT_LEFT
#define GIFT_TOP        210
#define GIFT_RIGHT      GIFT_LEFT + BUTTON_WIDTH
#define GIFT_BOTTOM     GIFT_TOP + BUTTON_HEIGHT

#define CLEAR           3
#define CANCEL          2
#define TIMEOUT         1
#define OK              0


//RECIEVE ERR CODE
#define 		ERR_OK							00			//成功
#define			ERR_UNKNOWN						01			//未知错误
#define			ERR_UNSUPPORTED					02			//不支持该方法或者属性
#define			ERR_LRC							03			//LRC错误
#define			ERR_PARAM						04			//参数非法或者参数个数不对
#define			ERR_FAIL						05			//失败
#define			ERR_USERCANCEL					06			//用户取消
#define			NO_PRINTER					    0xee		


typedef struct ST_UPOS_MSG
{
	uchar	PackageSize[2];									//数据包长度
	uchar	PackageType;									//数据包类型
	//uchar	CmdType[4];										//命令类型
}ST_UPOS_MSG;


typedef enum E_DATA_TYPE
{
	E_DATA_TYPE_CHAR	= 0X00,
	E_DATA_TYPE_SHORT	= 0X01,
	E_DATA_TYPE_LONG	= 0X02,
}E_DATA_TYPE;



void  Trade_Demo(void);
uchar Draw_SoftButton(int ButtonColor,int iLeft,int iTop,int iRight,int iBottom,int TextColor,uchar *pucText);
void  Credit_Trade(void);
void  Debit_Trade(void);
void  EBT_Trade(void);
void  Gift_Trade(void);
void  DisplayLogo(int iPosX,int iPosY);
void  DisplayPic(int iX,int iY,uchar *pucName);
void  ScreenProtect(void);
uchar ExitScreenProtect(void);
void  Display_Debit_Credit_PayPic(void);
uchar DisPlay_PurChase_Amount(void);
int   Display_EnterPin(void);
uchar Display_Signature(void);
void  Display_Authorization_Proc(void);
void  Display_Authorization_End(void);

int  XmlAddElement(uchar *xml_doc, int xml_doc_max_len, char *ele_name, uchar *ele_value, int value_len, int *xml_real_len);
uchar LoadingPng();
void DisplayAPPMenu();
#endif

